<?php

return [
    'required' => 'This field is required!',
    'integer' => 'This field must be an integer!',
    'numeric' => 'This field must be numeric!',
    'unique' => 'This field must be unique!',
    'image' => 'This field must be an image!',
];
