<?php

// ========== Custom Helpers ==========

if (! function_exists('array_has_keys')) {
    /**
     * @param array $keys
     * @param array $search
     * @return bool
     */
    function array_has_keys(array $keys, array $search): bool
    {
        foreach ($keys as $key) {
            if (is_string($key)) {
                if (! array_key_exists($key, $search)){
                    return false;
                }
            } else {
                return false;
            }
        }

        return true;
    }
}

if (! function_exists('relative_path_from_root')) {
    /**
     * @param string $path
     * @return string
     */
    function relative_path_from_root(string $path): string
    {
        if ($_SERVER['DOCUMENT_ROOT']) {
            $root = $_SERVER['DOCUMENT_ROOT'];
        } else {
            $root = getcwd() . '/public';
        }

        return realpath($root . '/' . preg_replace('/^\//', '', $path));
    }
}

if (! function_exists('assets_path')) {
    /**
     * @param string $path
     * @return string
     */
    function assets_path(string $path): string
    {
        $root = relative_path_from_root('/assets');

        return realpath($root . '/' . preg_replace('/^\//', '', $path));
    }
}

if (! function_exists('images_path')) {
    /**
     * @return string
     */
    function images_path(): string
    {
        $root = relative_path_from_root('/assets/images');

        return realpath($root);
    }
}

if (! function_exists('locale_path')) {
    /**
     * @return string
     */
    function locale_path(): string
    {
        $root = relative_path_from_root('/../locale');

        return realpath($root);
    }
}

if (! function_exists('logs_path')) {
    /**
     * @param string $file
     * @return string
     */
    function logs_path(): string
    {
        $root = relative_path_from_root('/../storage/logs');

        return realpath($root);
    }
}

use Ecommerce\Units\Library\Database\EloquentDatabase;
use Pecee\SimpleRouter\SimpleRouter as Router;

if (! function_exists('session')) {
    /**
     * @return \Ecommerce\Units\Session\Session
     */
    function session(): \Ecommerce\Units\Session\Session
    {
        return new \Ecommerce\Units\Session\Session();
    }
}

if (! function_exists('startsWith')) {
    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function startsWith(string $haystack, string $needle): bool
    {
        return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
    }
}

if (! function_exists('endsWith')) {
    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function endsWith(string $haystack, string $needle): bool
    {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }
}

// ========== Symphony dump Helper  ==========

if (!function_exists('dd')) {
    function dd()
    {
        $args = func_get_args();
        call_user_func_array('dump', $args);
        die;
    }
}

// ========== Eloquent Helpers ==========

if (!function_exists('db')) {
    function db(): EloquentDatabase
    {
        return new EloquentDatabase();
    }
}

if (!function_exists('connection')) {
    function connection()
    {
        return db()->getResolver()->getConnection();
    }
}

// ========== Route Helpers ==========

if (!function_exists('url')) {
    /**
     * Get url for a route by using either name/alias, class or method name.
     *
     * The name parameter supports the following values:
     * - Route name
     * - Controller/resource name (with or without method)
     * - Controller class name
     *
     * When searching for controller/resource by name, you can use this syntax "route.name@method".
     * You can also use the same syntax when searching for a specific controller-class "MyController@home".
     * If no arguments is specified, it will return the url for the current loaded route.
     *
     * @param string|null $name
     * @param string|array|null $parameters
     * @param array|null $getParams
     * @return \Pecee\Http\Url
     * @throws \InvalidArgumentException
     */
    function url(?string $name = null, $parameters = null, ?array $getParams = null): \Pecee\Http\Url
    {
        return Router::getUrl($name, $parameters, $getParams);
    }
}

if (!function_exists('response')) {
    /**
     * @return \Pecee\Http\Response
     */
    function response(): \Pecee\Http\Response
    {
        return Router::response();
    }
}

if (!function_exists('request')) {
    /**
     * @return \Pecee\Http\Request
     */
    function request(): \Pecee\Http\Request
    {
        return Router::request();
    }
}

if (!function_exists('input')) {
    /**
     * Get input class
     * @param string|null $index Parameter index name
     * @param string|null $defaultValue Default return value
     * @param array ...$methods Default methods
     * @return \Pecee\Http\Input\InputHandler|\Pecee\Http\Input\IInputItem|string
     */
    function input($index = null, $defaultValue = null, ...$methods)
    {
        if ($index !== null) {
            return request()->getInputHandler()->value($index, $defaultValue, ...$methods);
        }

        return request()->getInputHandler();
    }
}


if (!function_exists('redirect')) {
    /**
     * @param string $url
     * @param int|null $code
     */
    function redirect(string $url, ?int $code = null): void
    {
        if ($code !== null) {
            response()->httpCode($code);
        }
        response()->redirect($url);
    }
}

if (!function_exists('csrf_token')) {
    /**
     * Get current csrf-token
     * @return string|null
     */
    function csrf_token(): ?string
    {
        $baseVerifier = Router::router()->getCsrfVerifier();
        if ($baseVerifier !== null) {
            return $baseVerifier->getTokenProvider()->getToken();
        }
        return null;
    }
}
