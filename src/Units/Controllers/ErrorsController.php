<?php

declare(strict_types=1);

namespace Ecommerce\Units\Controllers;

use Ecommerce\Infrastructure\Domain\Controller\ViewController;
use Ecommerce\Units\Exceptions\Http\SessionException;
use Ecommerce\Units\Library\Http\Response\HttpCode;
use Ecommerce\Units\Library\Log\Logger;

/**
 * Class ErrorsController
 * @package Ecommerce\Units\Controllers
 */
class ErrorsController extends ViewController implements Contracts\ErrorsControllerContract
{
    /**
     * {@inheritDoc}
     */
    public function unauthorized(): void
    {
        $serverProtocol = request()->getHeader('server_protocol');
        header("{$serverProtocol} 401 Unauthorized");
        echo $this->template->render('ErrorHandling/401.html');
        exit;
    }

    /**
     * {@inheritDoc}
     */
    public function forbidden(): void
    {
        $serverProtocol = request()->getHeader('server_protocol');
        header("{$serverProtocol} 403 Forbidden");
        echo $this->template->render('ErrorHandling/403.html');
        exit;
    }

    /**
     * {@inheritDoc}
     */
    public function notFound(): void
    {
        $serverProtocol = request()->getHeader('server_protocol');
        header("{$serverProtocol} 404 Not Found");
        echo $this->template->render('ErrorHandling/404.html');
        exit;
    }

    /**
     * {@inheritDoc}
     */
    public function methodNotAllowed(): void
    {
        $serverProtocol = request()->getHeader('server_protocol');
        header("{$serverProtocol} 405 Method not allowed");
        echo $this->template->render('ErrorHandling/405.html');
        exit;
    }

    /**
     * {@inheritDoc}
     */
    public function unprocessableEntity(string $message): void
    {
        $origin = request()->getHeader('http_origin');
        $referer = request()->getHeader('http_referer');

        $next = str_replace($origin, '', $referer);

        $httpCode = HttpCode::HTTP_STATUS_UNPROCESSABLE_ENTITY;

        try {
            session()->writeFlash('validation', json_decode($message, true)['validation']);
            session()->writeFlash('data', json_decode($message, true)['data']);
            session()->writeFlash('message', ['type' => 'error', 'text' => 'Invalid data!']);
        } catch (SessionException $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            $httpCode = HttpCode::HTTP_STATUS_SERVICE_UNAVAILABLE;
        }

        response()->redirect($next, $httpCode);
    }

    /**
     * {@inheritDoc}
     */
    public function serviceUnavailable(): void
    {
        $serverProtocol = request()->getHeader('server_protocol');
        header("{$serverProtocol} 503 Service Unavailable");
        echo $this->template->render('ErrorHandling/503.html');
        exit;
    }
}
