<?php

declare(strict_types=1);

namespace Ecommerce\Units\Controllers\Contracts;

/**
 * Interface ErrorsControllerContract
 * @package Ecommerce\Units\Controllers\Contracts
 */
interface ErrorsControllerContract
{
    /**
     * @return void
     */
    public function unauthorized(): void;

    /**
     * @return void
     */
    public function forbidden(): void;

    /**
     * @return void
     */
    public function notFound(): void;

    /**
     * @return void
     */
    public function methodNotAllowed(): void;

    /**
     * @param string $message
     */
    public function unprocessableEntity(string $message): void;

    /**
     * @return void
     */
    public function serviceUnavailable(): void;
}
