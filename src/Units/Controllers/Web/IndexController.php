<?php

declare(strict_types=1);

namespace Ecommerce\Units\Controllers\Web;

use Ecommerce\Infrastructure\Domain\Controller\ViewController;

/**
 * Class IndexController
 * @package Ecommerce\Units\Controllers\Web
 */
class IndexController extends ViewController
{
    /**
     * @return string
     */
    public function index(): string
    {
        return $this->template->render('/pages/dashboard.twig');
    }
}