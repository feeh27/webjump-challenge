<?php

declare(strict_types=1);

namespace Ecommerce\Units\Controllers\Web;

use Ecommerce\Domain\Categories\Category;
use Ecommerce\Domain\Categories\Service\CategoryService;
use Ecommerce\Domain\ProductImage\Service\ProductImageService;
use Ecommerce\Domain\Products\Service\ProductService;
use Ecommerce\Infrastructure\Domain\Controller\ViewController;

/**
 * Class CategoryController
 * @package Ecommerce\Domain
 */
class ProductController extends ViewController
{
    /**
     * {@inheritDoc}
     */
    protected $serviceClass = ProductService::class;

    /**
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function index(): string
    {
		/**
		 * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
		 */
        $service = $this->newService();

		$query = url()->getParams();

		$service->setFilters($query);

        /**
         * @var \Illuminate\Support\Collection $products
         */
        $products = $service->get()['data'];

        $data = [
            'products' => $products,
        ];

        $data = $this->getFlashData($data);

        return $this->template->render('/pages/products.twig', $data);
    }

    /**
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function new(): string
    {
        $categoryService = new CategoryService();

        /**
         * @var \Illuminate\Support\Collection $categoriesCollection
         */
        $categoriesCollection = $categoryService->get()['data'];

        $categories = $categoriesCollection->filter(function (Category $category) {
            return ! ($category->name === '(no category listed)' || $category->name === '(no genres listed)');
        });

        $data = [
            'categories' => $categories,
            'action' => '/products/save',
        ];

        $data = $this->getFlashData($data);

        return $this->template->render('/pages/form-product.twig', $data);
    }

    /**
     * @param $id
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function edit($id): string
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();
        $categoryService = new CategoryService();

        /**
         * @var \Ecommerce\Domain\Products\Product $product;
         */
        $product = $service->getById($id);

        /**
         * @var \Illuminate\Support\Collection $categoriesCollection
         */
        $categoriesCollection = $categoryService->get()['data'];

        $categories = $categoriesCollection->filter(function (Category $category) {
            return ! ($category->name === '(no category listed)' || $category->name === '(no genres listed)');
        });

        $productCategories = $product->categories->pluck('id')->toArray();

        $piService = new ProductImageService();

        $image = $piService->getByProductIdAndName((int) $id, 'profile');

        $data = [
            'action' => '/products/' . $id . '/save',
            'product' => $product,
            'image' => $image,
            'categories' => $categories,
            'product_categories' => $productCategories,
        ];

        $data = $this->getFlashData($data);

        return $this->template->render('/pages/form-product.twig', $data);
    }

    /**
     * @param int|string|null $id
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function save($id = null)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\CrudService $service
         */
        $service = $this->newService();

        $data = input()->all(['sku', 'name', 'price', 'quantity', 'categories', 'description']);

        $image = input()->file('image', null);

        $data['image'] = $image;

        if ($id === null) {
            $product = $service->create($data);
        } else {
            $product = $service->update($id, $data);
        }

        $httpCode = 200;

        $message = [];

        if ($product) {
            $message['type'] = 'success';

            if ($id === null) {
                $message['text'] = 'Product created successfully!';
            } else {
                $message['text'] = 'Product updated successfully!';
            }
        } else {
            $message['type'] = 'error';

            if ($id === null) {
                $message['text'] = 'Error creating product!';
            } else {
                $message['text'] = 'Error updating product!';
            }

            $httpCode = 503;
        }

        session()->writeFlash('message', $message);

        response()->redirect(url('products')->getPath(), $httpCode);
    }

    /**
     * @param $id
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function delete($id)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\CrudService $service
         */
        $service = $this->newService();

        $deleted = $service->delete($id);

        $httpCode = 200;

        $message = [];

        if ($deleted) {
            $message['type'] = 'success';
            $message['text'] = 'Product deleted successfully!';
        } else {
            $message['type'] = 'error';
            $message['text'] = 'Error deleting product!';
            $httpCode = 503;
        }

        session()->writeFlash('message', $message);

        response()->redirect(url('products')->getPath(), $httpCode);
    }
}
