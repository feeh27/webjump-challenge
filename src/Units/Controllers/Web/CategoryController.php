<?php

declare(strict_types=1);

namespace Ecommerce\Units\Controllers\Web;

use Ecommerce\Domain\Categories\Service\CategoryService;
use Ecommerce\Infrastructure\Domain\Controller\ViewController;

/**
 * Class CategoryController
 * @package Ecommerce\Domain
 */
class CategoryController extends ViewController
{
    /**
     * {@inheritDoc}
     */
    protected $serviceClass = CategoryService::class;

    /**
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function index(): string
    {
		/**
		 * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
		 */
		$service = $this->newService();

		$query = url()->getParams();

		$service->setFilters($query);

        $categoriesCollection = $service->get()['data'];

        $categories = $categoriesCollection;

        $data = [
            'categories' => $categories,
        ];

        $data = $this->getFlashData($data);

        return $this->template->render('/pages/categories.twig', $data);
    }

    /**
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function new(): string
    {
        $data = [
            'action' => '/categories/save',
        ];

        $data = $this->getFlashData($data);

        return $this->template->render('/pages/form-category.twig', $data);
    }

    /**
     * @param $id
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function edit($id): string
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $category = $service->getById($id);

        $data = [
            'action' => '/categories/' . $id . '/save',
            'category' => $category,
        ];

        $data = $this->getFlashData($data);

        return $this->template->render('/pages/form-category.twig', $data);
    }

    /**
     * @param int|string|null $id
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function save($id = null)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\CrudService $service
         */
        $service = $this->newService();

        $data = input()->all(['name', 'code']);

        if ($id === null) {
            $model = $service->create($data);
        } else {
            $model = $service->update($id, $data);
        }

        $httpCode = 200;

        $message = [];

        if ($model) {
            $message['type'] = 'success';

            if ($id === null) {
                $message['text'] = 'Category created successfully!';
            } else {
                $message['text'] = 'Category updated successfully!';
            }
        } else {
            $message['type'] = 'error';

            if ($id === null) {
                $message['text'] = 'Error creating category!';
            } else {
                $message['text'] = 'Error updating category!';
            }

            $httpCode = 503;
        }

        session()->writeFlash('message', $message);

        response()->redirect(url('categories')->getPath(), $httpCode);
    }

    /**
     * @param $id
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function delete($id)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\CrudService $service
         */
        $service = $this->newService();

        $deleted = $service->delete($id);

        $httpCode = 200;

        $message = [];

        if ($deleted) {
            $message['type'] = 'success';
            $message['text'] = 'Category deleted successfully!';
        } else {
            $message['type'] = 'error';
            $message['text'] = 'Error deleting category!';
            $httpCode = 503;
        }

        session()->writeFlash('message', $message);

        response()->redirect(url('categories')->getPath(), $httpCode);
    }
}
