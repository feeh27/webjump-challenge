<?php

declare(strict_types=1);

namespace Ecommerce\Units\Exceptions\Http;

use Pecee\SimpleRouter\Exceptions\HttpException;

/**
 * Class MethodNotAllowedException
 * @package Ecommerce\Units\Exceptions\Http
 */
class MethodNotAllowedException extends HttpException
{

}