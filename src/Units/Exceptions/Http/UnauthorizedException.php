<?php

declare(strict_types=1);

namespace Ecommerce\Units\Exceptions\Http;

use Pecee\SimpleRouter\Exceptions\HttpException;

/**
 * Class UnauthorizedException
 * @package Ecommerce\Units\Exceptions
 */
class UnauthorizedException extends HttpException
{

}