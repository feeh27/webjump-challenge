<?php

declare(strict_types=1);

namespace Ecommerce\Units\Exceptions\Http;

use Pecee\SimpleRouter\Exceptions\HttpException;

/**
 * Class ValidationExceptionHandler
 * @package Ecommerce\Units\Exceptions
 */
class ValidationException extends HttpException
{

}
