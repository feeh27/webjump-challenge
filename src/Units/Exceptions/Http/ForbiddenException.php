<?php

declare(strict_types=1);

namespace Ecommerce\Units\Exceptions\Http;

use Pecee\SimpleRouter\Exceptions\HttpException;

/**
 * Class ForbiddenException
 * @package Ecommerce\Units\Exceptions\Http
 */
class ForbiddenException extends HttpException
{

}