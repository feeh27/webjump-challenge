<?php

declare(strict_types=1);

namespace Ecommerce\Units\Exceptions;

use Ecommerce\Units\Controllers\ErrorsController;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pecee\Http\Request;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;
use Pecee\SimpleRouter\Handlers\IExceptionHandler;

/**
 * Class ExceptionHandler
 * @package Ecommerce\Units\Exceptions
 */
class ExceptionHandler implements IExceptionHandler
{
    /**
     * @param \Pecee\Http\Request $request
     * @param Exception $error
     */
    public function handleError(Request $request, Exception $error): void
    {
        dd($error);

        $exceptionHandler = $this->getErrorControllerMethod($error);

        if ($exceptionHandler === 'Ecommerce\Units\Controllers\ErrorsController@unprocessableEntity') {
            $errorsController = new ErrorsController();
            $errorsController->unprocessableEntity($error->getMessage());
        }

        $request->setRewriteCallback($exceptionHandler);

        return;
    }

    /**
     * @param Exception $error
     * @return string
     */
    public function getErrorControllerMethod(Exception $error): string
    {
        if ($error instanceof Http\UnauthorizedException) {
            $exceptionHandler = 'Ecommerce\Units\Controllers\ErrorsController@unauthorized';
        } else if ($error instanceof Http\ForbiddenException) {
            $exceptionHandler = 'Ecommerce\Units\Controllers\ErrorsController@forbidden';
        } else if ($error instanceof NotFoundHttpException || $error instanceof ModelNotFoundException) {
            $exceptionHandler = 'Ecommerce\Units\Controllers\ErrorsController@notFound';
        } else if ($error instanceof Http\MethodNotAllowedException) {
            $exceptionHandler = 'Ecommerce\Units\Controllers\ErrorsController@methodNotAllowed';
        } else if ($error instanceof Http\ValidationException) {
            $exceptionHandler = 'Ecommerce\Units\Controllers\ErrorsController@unprocessableEntity';
        } else {
            $exceptionHandler = 'Ecommerce\Units\Controllers\ErrorsController@serviceUnavailable';
        }

        return $exceptionHandler;
    }

}
