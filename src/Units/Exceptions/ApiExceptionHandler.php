<?php

declare(strict_types=1);

namespace Ecommerce\Units\Exceptions;

use Ecommerce\Units\Library\Http\Response\HttpCode;
use Exception;
use Pecee\Http\Request;
use Pecee\SimpleRouter\Handlers\IExceptionHandler;

/**
 * Class ExceptionHandler
 * @package Ecommerce\Units\Exceptions
 */
class ApiExceptionHandler implements IExceptionHandler
{
    /**
     * @param \Pecee\Http\Request $request
     * @param Exception $error
     */
    public function handleError(Request $request, Exception $error): void
    {
        $code = $error->getCode();
        $httpCode = new HttpCode();

        if (! ($httpCode->isClientError($code) || $httpCode->isServerError($code))) {
            $code = 200;
        }

        response()->httpCode($code)->json([
            'code'  => $error->getCode(),
            'error' => str_replace('"', '\'', $error->getMessage()),
            'data'  => [],
        ]);
    }
}
