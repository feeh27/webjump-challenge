<?php

declare(strict_types=1);

namespace Ecommerce\Units\Console\Commands\Migrator;

use Ecommerce\Units\Console\Commands\CommandFormatter;
use Ecommerce\Units\Library\Database\Migration\Migrator;
use Ecommerce\Units\Library\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Migrator
 * @package Ecommerce\Units\Console\Commands
 */
class Migrate extends Command
{
    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('migrator:migrate');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $outputFormatter = new CommandFormatter();
        $outputFormatter->setOutputStyles($output);

        $logger = new Logger();

        $output->writeln('<comment>Start migrations...</comment>');
        $logger->logMigrator('Start migrations...', 'migrate', self::class);

        $migrator = new Migrator();
        $migrations = $migrator->migrate();

        if (count($migrations) > 0) {
            foreach ($migrations as $migration) {
                $output->writeln('<highlight>Migrate: '. $migration.'</highlight>');
                $logger->logMigrator('Migrate', 'migrate', $migration);
            }
        } else {
            $output->writeln('<highlight-bold>Nothing to migrate!</highlight-bold>');
            $logger->logMigrator('Nothing to migrate!', 'migrate', self::class);
        }

        $output->writeln('<comment>Migrations completed successfully!</comment>');
        $logger->logMigrator('Migrations completed successfully!', 'migrate', self::class);
    }
}