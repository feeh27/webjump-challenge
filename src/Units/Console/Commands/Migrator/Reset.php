<?php

declare(strict_types=1);

namespace Ecommerce\Units\Console\Commands\Migrator;

use Ecommerce\Units\Console\Commands\CommandFormatter;
use Ecommerce\Units\Library\Database\Migration\Migrator;
use Ecommerce\Units\Library\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Migrator
 * @package Ecommerce\Units\Console\Commands
 */
class Reset extends Command
{
    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('migrator:reset');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $outputFormatter = new CommandFormatter();
        $outputFormatter->setOutputStyles($output);

        $logger = new Logger();

        $output->writeln('<comment>Start reset...</comment>');
        $logger->logMigrator('Start reset...', 'reset', self::class);

        $migrator = new Migrator();
        $migrations = $migrator->reset();

        if (count($migrations) > 0) {
            foreach ($migrations as $migration) {
                $output->writeln('<highlight>Reset: '. $migration.'</highlight>');
                $logger->logMigrator('Reset', 'reset', $migration);
            }
        } else {
            $output->writeln('<highlight-bold>No migration registered!</highlight-bold>');
            $logger->logMigrator('No migration registered!', 'reset', self::class);
        }

        $output->writeln('<comment>Migrations reset completed successfully!</comment>');
        $logger->logMigrator('Migrations reset completed successfully!', 'reset', self::class);
    }
}