<?php

declare(strict_types=1);

namespace Ecommerce\Units\Console\Commands\Migrator;

use Ecommerce\Units\Console\Commands\CommandFormatter;
use Ecommerce\Units\Library\Database\Migration\Migrator;
use Ecommerce\Units\Library\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Migrator
 * @package Ecommerce\Units\Console\Commands
 */
class Rollback extends Command
{
    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('migrator:rollback');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $outputFormatter = new CommandFormatter();
        $outputFormatter->setOutputStyles($output);

        $logger = new Logger();

        $output->writeln('<comment>Start rollbacks...</comment>');
        $logger->logMigrator('Start rollbacks...', 'rollback', self::class);

        $migrator = new Migrator();
        $migrations = $migrator->rollback();

        if (count($migrations) > 0) {
            foreach ($migrations as $migration) {
                $output->writeln('<highlight>Rollback: '. $migration.'</highlight>');
                $logger->logMigrator('Rollback', 'rollback', $migration);
            }
        } else {
            $output->writeln('<highlight-bold>Nothing to rollback!</highlight-bold>');
            $logger->logMigrator('Nothing to rollback!', 'rollback', self::class);
        }

        $output->writeln('<comment>Rollbacks completed successfully!</comment>');
        $logger->logMigrator('Rollbacks completed successfully!', 'rollback', self::class);
    }
}