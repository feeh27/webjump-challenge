<?php

declare(strict_types=1);

namespace Ecommerce\Units\Console\Commands\DataImport;

use Ecommerce\Domain\Products\ValueObjects\DataImport\ProductCsvHandler;
use Ecommerce\Units\Console\Commands\CommandFormatter;
use Ecommerce\Units\Library\File\Csv\CsvHandler;
use Ecommerce\Units\Library\Log\Logger;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Migrator
 * @package Ecommerce\Units\Console\Commands
 */
class CsvImport extends Command
{
    /**
     * @var string
     */
    private $command = 'import:csv';

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('import:csv');

        $name = 'model';
        $shortcut = null;
        $mode = InputOption::VALUE_REQUIRED;
        $description = '';
        $default = '';

        $this->addOption($name, $shortcut, $mode, $description, $default);

        $name = 'offset';
        $shortcut = null;
        $mode = InputOption::VALUE_OPTIONAL;
        $description = '';
        $default = 0;

        $this->addOption($name, $shortcut, $mode, $description, $default);

        $name = 'limit';
        $shortcut = null;
        $mode = InputOption::VALUE_OPTIONAL;
        $description = '';
        $default = 0;

        $this->addOption($name, $shortcut, $mode, $description, $default);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $outputFormatter = new CommandFormatter();
        $outputFormatter->setOutputStyles($output);

        $modelClass = $this->validateParameters($input, $output)['modelClass'];
        $model  = $this->validateParameters($input, $output)['model'];
        $offset = $this->validateParameters($input, $output)['offset'];
        $limit  = $this->validateParameters($input, $output)['limit'];

        $this->command = 'import:csv --model=' . $modelClass . ' --offset=' . $offset . ' --limit=' . $limit;

        $rows = 0;
        $registers = 0;
        $success = false;

        $output->writeln('<comment>Start import...</comment>');

        $logger = new Logger();
        $logger->logImport('Start import', $modelClass, $this->command,'info');

        try {
            $csv = new CsvHandler($model, 'r');
            $processResult = $csv->process($offset, $limit);
            $rows      = $processResult->getRows();
            $registers = $processResult->getRegisters();
            $success = true;
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
        }

        $this->output($output, $success, $rows, $registers, $modelClass);
    }

	/**
	 * @param OutputInterface $output
	 * @param bool   $success
	 * @param int    $rows
	 * @param int    $registers
	 * @param string $model
	 */
    public function output(OutputInterface $output, bool $success, int $rows, int $registers, string $model): void
	{
        $logger = new Logger();

		if ($success) {
			if ($rows > 0) {
				$output->writeln('<highlight>Processed ' . $rows . ' row(s)!</highlight>');

				$output->writeln('<highlight>Inserted ' . $registers . ' register(s)!</highlight>');

                $logger->logImport('Processed ' . $rows . ' row(s)!', $model, $this->command, 'info');
                $logger->logImport('Inserted ' . $registers . ' register(s)!', $model, $this->command, 'info');

				$diff = $rows - $registers;

				if ($diff !== 0) {
				    $message = $diff . ' row(s) not inserted';
					$msg = $message . ', check log file in storage/logs/import/product-errors.log!';
					$output->writeln('<red-bold-backlight>' . $msg . '</red-bold-backlight>');

                    $logger->logImport($message . '!', $model, $this->command, 'partial');
				} else {
				    $message = 'Import completed successfully!';
                    $logger->logImport($message, $model, $this->command, 'success');
                }

				$output->writeln('<comment>Import completed successfully!</comment>');
			} else {
				$output->writeln('<highlight-bold>Nothing to import!</highlight-bold>');
                $logger->logImport('Nothing to import!', $model, $this->command, 'info');
			}
		} else {
            $message = 'Error importing data';
			$output->writeln('<error>' . $message . ', check log file in storage/logs/import/product-errors.log!</error>');

            $logger->logImport($message, $model, $this->command,'error');
		}
	}

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
	protected function validateParameters(InputInterface $input, OutputInterface $output): array
    {
        $parameters = [];

        $modelClass = $input->getOption('model');
        $offset = (int) $input->getOption('offset');
        $limit = (int) $input->getOption('limit');

        if ($modelClass === null) {
            $output->writeln('<error>Missing model parameter!</error>');
            exit;
        }

        if (empty($modelClass)) {
            $output->writeln('<error>Model parameter can\'t be empty!</error>');
            exit;
        } else if ($modelClass === 'product') {
            $parameters['model'] = new ProductCsvHandler();
            $parameters['modelClass'] = $modelClass;
        } else {
            $output->writeln('<error>Invalid model option!</error>');
            exit;
        }

        $parameters['offset'] = $offset;
        $parameters['limit']  = $limit;

        if ($limit === 0) {
            $parameters['limit'] = null;
        }

        return $parameters;
    }
}
