<?php

declare(strict_types=1);

namespace Ecommerce\Units\Console\Commands;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CommandFormatter
 * @package Ecommerce\Units\Console\Commands
 */
class CommandFormatter
{
    /**
     * @param OutputInterface $output
     */
    public function setOutputStyles(OutputInterface $output): void
    {
        // Background Colors: black, red, green, yellow, blue, magenta, cyan, white, default

        $highlight = new OutputFormatterStyle('blue', 'default', []);
        $output->getFormatter()->setStyle('highlight', $highlight);

        $highlightBold = new OutputFormatterStyle('blue', 'default', ['bold']);
        $output->getFormatter()->setStyle('highlight-bold', $highlightBold);

        $redBoldBacklight = new OutputFormatterStyle('red', 'black', ['bold']);
        $output->getFormatter()->setStyle('red-bold-backlight', $redBoldBacklight);
    }
}