<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Repository;

use Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract;
use Ecommerce\Units\Library\Log\Logger;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class EloquentCrudRepository
 * @package Ecommerce\Units\Library\Repository
 */
class EloquentCrudRepository extends EloquentRepository implements CrudRepositoryContract
{
    /**
     * @return array | \Illuminate\Support\Collection
     */
    public function get()
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Model\Model $model
         */
        $model = new $this->modelClass;

        $filters = $this->filters;

        $query = $this->prepareQuery($filters);

        $repositoryFilter = new EloquentRepositoryFilter($model->getKeyName(), $filters);

		$take = $repositoryFilter->getLimit();
		$paginate = false;
		// Paginação pelo Eloquent desabilitada
		// $paginate = $repositoryFilter->isPaginate();

		$sort  = $repositoryFilter->getSort();
		$order = $repositoryFilter->getOrder();

		/**
		 * @var \Illuminate\Database\Eloquent\Builder $query
		 */
        $query = $query->orderBy($sort, $order);

        $result = $this->doQuery($query, $take, $paginate);

        $response = [];

        if ($result instanceof LengthAwarePaginator) {
            $response = $this->handlePagination($result, $take);
        } else {
            $response['data'] = $result;
        }

        return $response;
    }

	/**
	 * @param \Illuminate\Pagination\LengthAwarePaginator $paginated
	 * @param null|int $take
	 * @return array
	 */
    public function handlePagination(LengthAwarePaginator $paginated, ?int $take): array
	{
		$result = $paginated->toArray();

		$response = [];

		$response['data'] = $result['data'];
		unset($result['data']);

		$first = $result['first_page_url'];
		$next  = $result['next_page_url'];
		$prev  = $result['prev_page_url'];
		$last  = $result['last_page_url'];

		$limit_query = "&limit=".$take;

		$response['pagination'] = $result;

		$response['pagination']['first_page_url'] = empty($first) ? '' : $first.$limit_query;
		$response['pagination']['next_page_url']  = empty($next)  ? '' : $next .$limit_query;
		$response['pagination']['prev_page_url']  = empty($prev)  ? '' : $prev .$limit_query;
		$response['pagination']['last_page_url']  = empty($last)  ? '' : $last .$limit_query;

		return $response;
	}

    /**
     * @param int|string $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getById($id): Model
    {
        /**
         * @var \Illuminate\Database\Eloquent\Builder $query
         */
        $query = $this->newQuery();

        return $query->findOrFail($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getFirst(): Model
    {
        /**
         * @var \Illuminate\Database\Eloquent\Builder $query
         */
        $query = $this->prepareQuery($this->filters);

        return $query->firstOrFail();
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = []): Model
    {
        $model = $this->factory($data);

        return $this->save($model);
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function firstOrCreate(array $data): Model
    {
		/**
		 * @var \Ecommerce\Infrastructure\Domain\Model\Model $model
		 */
        $model = $this->newModel();

        return $model->firstOrCreate($data);
    }

    /**
     * @param int|string $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data = []): Model
    {
        $model = $this->getById($id);

        $this->fillModel($model, $data);

        return $this->save($model);
    }

    /**
     * Caso o registro exista atualiza o mesmo, caso não exista cria um novo
     * Em caso de sucesso retorna o objeto criado / atualizado
     * Em caso de falhas retorna um objeto vazio
     *
     * @param array $filters
     * @param array $data
     * @return Model
     */
    public function updateOrCreate(array $filters, array $data): Model
    {
        $query = $this->newQuery();

        $this->setFilters($filters);

        $model = $this->getFirst();

        if ($query->updateOrInsert($filters, $data)) {
            return $this->getById($model->id);
        } else {
            return new $this->modelClass;
        }
    }

    /**
     * Deleta o registro cujo o id seja o mesmo do Model passado por parâmetro.
     *
     * @param int|string $id
     * @return bool
     */
    public function delete($id): bool
    {
        try {
            $model = $this->getById($id);
            $model->delete();
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            return false;
        }

        return true;
    }

    /**
     * Restaura o registro com o $id passado por parâmetro,
     * caso esse $id não exista retorna um not found exception.
     *
     * @param int|string $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function restore($id): Model
    {
        $query = $this->newQuery();

        $query->withTrashed()->findOrFail($id)->restore();

        return $this->getById($id);
    }

    /**
     * @param array $filters
     * @return \Illuminate\Database\Query\Builder
     */
    protected function prepareQuery(array $filters = [])
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Model\Model $model
         */
        $model = new $this->modelClass;

        $query = $this->newQuery();

        foreach ($filters as $key => $value) {
            if (!in_array($key, $model->getEnabledFilters())) {
                unset($filters[$key]);
            }
        }

        // Verifica se há filtros
        if ((count($filters) > 0)) {
            $query = $query->where($filters);
        }

        return $query;
    }
}
