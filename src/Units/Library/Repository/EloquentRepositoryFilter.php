<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Repository;

/**
 * Class EloquentRepositoryFilter
 * @package Ecommerce\Units\Library\Repository
 */
class EloquentRepositoryFilter
{
	/**
	 * @var string
	 */
	protected $keyName;

	/**
	 * @var array
	 */
	protected $filters = [];

	/**
	 * @var int|null
	 */
	protected $limit;

	/**
	 * @var bool
	 */
	protected $paginate = false;

	/**
	 * @var string
	 */
	protected $sort;

	/**
	 * @var string
	 */
	protected $order = 'ASC';

	/**
	 * EloquentRepositoryFilter constructor.
	 * @param string $keyName
	 * @param array $filters
	 */
	public function __construct(string $keyName, array $filters = [])
	{
		$this->keyName = $keyName;
		$this->filters = $filters;
	}

	/**
	 * @return null|int
	 */
	public function getLimit(): ?int
	{
		if (isset($this->filters['limit'])) {
			$limit = (int) $this->filters['limit'];

			// Verifica se o parâmetro limit é númerico e maior que zero
			if (is_numeric($limit) && $limit > 0) {
				$this->limit = $limit;
			}
		}

		return $this->limit;
	}

	/**
	 * @return bool
	 */
	public function isPaginate(): bool
	{
		if (isset($this->filters['page']) && $this->filters['page'] !== null) {
			$this->paginate = true;
		}

		return $this->paginate;
	}

	/**
	 * @return string
	 */
	public function getSort(): string
	{
		if (isset($this->filters['sort'])) {
			$this->sort = $this->filters['sort'];
		} else {
			$this->sort = $this->keyName;
		}

		return $this->sort;
	}

	/**
	 * @return string
	 */
	public function getOrder(): string
	{
		$orders = ['ASC', 'DESC', 'asc', 'desc'];

		if (isset($this->filters['order']) && in_array($this->filters['order'], $orders)) {
			$this->order = $this->filters['order'];
		}

		return $this->order;
	}
}
