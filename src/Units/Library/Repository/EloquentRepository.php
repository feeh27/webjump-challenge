<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Repository;

use Ecommerce\Infrastructure\Core\BaseRepository;
use Ecommerce\Infrastructure\Domain\Repository\Contracts\RepositoryContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EloquentRepository
 * @package Ecommerce\Units\Library\Repository
 */
class EloquentRepository extends BaseRepository implements RepositoryContract
{
    /**
     * Classe do modelo
     *
     * @var string
     */
    protected $modelClass = Model::class;

    /**
     * Nome do banco de dados
     *
     * @var string
     */
    protected $connection;

    /**
     * @var null|\Ecommerce\Units\Library\Database\EloquentDatabase
     */
    protected $database;

    /**
     * @var null|\Illuminate\Database\Connection
     */
    protected $eloquentConnection;

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * EloquentRepository constructor.
     * @param string $modelClass
     * @param string $connection
     */
    public function __construct(string $modelClass, string $connection)
    {
        $this->modelClass = $modelClass;

        $this->connection = $connection;

        $this->database = db();

        $this->database->config();

        $this->eloquentConnection = $this->database->getResolver()->getConnection();
    }

    /**
     * DataImportRepository destructor
     */
    public function __destruct()
    {
        $this->eloquentConnection->disconnect();
    }

    /**
     * @param array $filters
     * @return void
     */
    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    /**
     * Popula a model com o array, ambos são passados por parâmetro.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function fillModel(Model $model, array $data = []): Model
    {
        $model->fill($data);
        return $model;
    }

    /**
     * Cria uma instância do queryBuilder do Laravel
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newQuery(): Builder
    {
        /**
         * @var \Illuminate\Database\Eloquent\Model $model
         */
        $model = $this->newModel();

        /**
         * @var \Illuminate\Database\Eloquent\Builder $builder
         */
        $builder = $model->on($this->connection)->newQuery();

        return $builder;
    }

    /**
     * Faz um (select * from table) no banco.
     *
     * Se o parâmetro $builder não é nulo, é feito uma nova instância do queryBuilder.
     * Se o parâmetro $paginate é falso ele carrega os dados sem paginação, se é verdadeiro os dados são paginados.
     * Se o parâmetro $take é falso, todos os registros do banco são carregados, caso contrário são carregados o
     *   número de registros passados em $take.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $take
     * @param bool $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator | \Illuminate\Database\Eloquent\Builder[] | \Illuminate\Support\Collection
     */
    protected function doQuery(Builder $query = null, int $take = null, bool $paginate = false)
    {
        if (!$query) $query = $this->newQuery();
        if ($paginate) return $query->paginate($take);
        if ($take !== null) return $query->take($take)->get();

        return $query->get();
    }

    /**
     * Cria uma nova instância da Model cujo nome é passado em $this->modelClass e
     * popula com os dados do $array passado.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function factory(array $data = []): Model
    {
        /**
         * @var \Illuminate\Database\Eloquent\Builder $query
         */
        $query = $this->newQuery();

        $model = $query->getModel();

        if ($model instanceof Model) {
            $this->fillModel($model, $data);
        }

        return $model;
    }

    /**
     * Salva no banco o Model passado como parâmetro como um novo registro.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save(Model $model): Model
    {
        $model->save();
        return $model;
    }

    /**
     * @param callable $callback
     * @return bool
     * @throws \Exception
     */
    public function transaction(callable $callback): bool
    {
        $this->eloquentConnection->beginTransaction();

        if (!$callback()) {
            $this->eloquentConnection->rollback();
            return false;
        }

        $this->eloquentConnection->commit();

        return true;
    }
}