<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Validation;

use Ecommerce\Units\Exceptions\Http\ValidationException;
use Ecommerce\Units\Library\Database\EloquentDatabaseResolver;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\DatabasePresenceVerifier;
use Illuminate\Validation\Factory;

/**
 * Class Validator
 * @package Ecommerce\Units\Library\Validation
 */
class Validator
{
    /**
     * @var Factory
     */
    protected $factory;

    /**
     * Validator constructor.
     */
    public function __construct()
    {
        $locale = getenv('locale') ? getenv('locale') : 'en_US';
        $filesystem = new Filesystem();
        $fileLoader = new FileLoader($filesystem, locale_path());
        $translator = new Translator($fileLoader, $locale);
        $this->factory = new Factory($translator);

        $dbResolver = new EloquentDatabaseResolver();
        $dbPresenceVerifier = new DatabasePresenceVerifier($dbResolver);
        $this->factory->setPresenceVerifier($dbPresenceVerifier);
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @return array
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function validate(array $data, array $rules, array $messages = [], array $customAttributes = []): array
    {
        /**
         * @var \Illuminate\Validation\Validator $validator
         */
        $validator = $this->make($data, $rules, $messages, $customAttributes);

        $errors = [];

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            $message = json_encode(['data' => $data, 'validation' => $errors]);
            throw new ValidationException($message, 422);
        }

        return $errors;
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @return \Illuminate\Validation\Validator
     */
    public function make(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        /**
         * @var \Illuminate\Validation\Validator $validator
         */
        $validator = $this->factory->make($data, $rules, $messages, $customAttributes);

        return $validator;
    }
}