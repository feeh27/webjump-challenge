<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Render;

use Ecommerce\Units\Library\Log\Logger;
use Exception;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class Render
 * @package Ecommerce\Units\Library\Render
 */
class Template
{
    /**
     * @var \Twig\Environment $twig
     */
    protected $twig;

    /**
     * Template constructor.
     * @param array $options
     */
    public function __construct($options = [])
    {
        try {
            $loader = new FilesystemLoader(__DIR__ . '/../../../Views/');
            $this->twig = new Environment($loader, $options);
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            $this->handleException($e);
        }
    }

    /**
     * @param string $name
     * @param array $context
     * @return string
     */
    public function render(string $name, array $context = [])
    {
        try {
            return $this->twig->render($name, $context);
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            return $this->handleException($e);
        }
    }

    /**
     * @return mixed|string
     */
    public function handleNotFound()
    {
        try {
            return $this->twig->render('ErrorHandling/404.html');
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            return $this->handleException($e);
        }
    }

    /**
     * @return string
     */
    public function handleMethodNotAllowed()
    {
        try {
            return $this->twig->render('ErrorHandling/405.html');
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            return $this->handleException($e);
        }
    }

    /**
     * @param Exception $e
     * @return mixed
     */
    public function handleException(Exception $e)
    {
        try {
            $exception = [
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace(),
                'previous' => $e->getPrevious(),
            ];

            return $this->twig->render('ErrorHandling/Exception.html', $exception);
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logException(self::class, $e);
            return $this->error();
        }
    }

    /**
     * Erro padrão
     */
    protected function error()
    {
        return (include_once __DIR__ . '../../../Views/ErrorHandling/Error.html');
    }
}