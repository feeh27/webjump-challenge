<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Log;

use Ecommerce\Units\Library\Http\Response\HttpCode;
use Exception;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;

/**
 * Class Logger
 * @package Ecommerce\Units\Library\Log
 */
class Logger
{
    /**
     * @param string $class
     * @param Exception $exception
     */
    public function logException(string $class, Exception $exception)
    {
        $code = $exception->getCode();

        $filepath = logs_path().'/exceptions/' . $code . 'error.log';

        $logger = $this->getLogger('db_actions', $filepath);

        $context = [
            'class' => $class,
            'exception' => $exception,
        ];

        if (HttpCode::isServerError($code)) {
            $logger->error('Error', $context);
        } else {
            $logger->critical('Critical', $context);
        }
    }

    /**
     * @param string $message
     * @param string $modelClass
     * @param string $action
     * @param array $data
     */
    public function logDbActions(string $message, string $modelClass, string $action, array $data)
    {
        $modelArray = explode('\\', $modelClass);
        $model = strtolower($modelArray[count($modelArray)-1]);

        $filepath = logs_path().'/actions/' . strtolower($action) . '_' . $model . '.log';

        $logger = $this->getLogger('db_actions', $filepath);

        $context = [
            'model'  => $modelClass,
            'action' => $action,
            'data'   => $data,
        ];

        $logger->notice($message, $context);
    }

    /**
     * @param string $message
     * @param string $action
     * @param string $class
     * @param int $batch
     */
    public function logMigrator(string $message, string $action, string $class)
    {
        $filepath = logs_path().'/migrations/migrations.log';
        $logger = $this->getLogger('db_migrations', $filepath);

        $context = [
            'action' => $action,
            'class'  => $class,
        ];

        $logger->notice($message, $context);
    }

    /**
     * The status can be: info, success, partial, failure
     * The types can be: csv, xml
     *
     * @param string $message
     * @param string $model
     * @param string $command
     * @param string $status
     */
    public function logImport(string $message, string $model, string $command, string $status = 'success')
    {
        $filepath = logs_path().'/import/' . $model . '.log';
        $logger = $this->getLogger('import', $filepath);

        $context = [
            'command' => $command,
            'status' => $status,
        ];

        if (in_array($status, ['info'])) {
            $logger->info($message);
        } else if (in_array($status, ['success', 'partial'])) {
            $logger->notice($message, $context);
        } else if (in_array($status, ['error'])) {
            $logger->error($message, $context);
        }
    }

    /**
     * @param string $model
     * @param string $class
     * @param Exception $exception
     */
    public function logImportException(string $model, string $class, Exception $exception)
    {
        $filepath = logs_path().'/import/' . $model . '.log';
        $logger = $this->getLogger('import', $filepath);

        $context = [
            'model' => $model,
            'class' => $class,
            'exception' => $exception,
        ];

        $logger->error('Error', $context);
    }

    /**
     * @param string $channel
     * @param string $filepath
     * @param int $level
     * @return \Monolog\Logger
     */
    protected function getLogger(string $channel, string $filepath, $level = \Monolog\Logger::DEBUG): \Monolog\Logger
    {
        try {
            $stream  = new StreamHandler($filepath, $level);
            $firephp = new FirePHPHandler();
        } catch (Exception $e) {
            die('Log failure: ' . $e->getMessage());
        }

        $logger = new \Monolog\Logger($channel);
        $logger->pushHandler($stream);
        $logger->pushHandler($firephp);

        return $logger;
    }
}
