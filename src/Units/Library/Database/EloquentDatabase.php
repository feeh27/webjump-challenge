<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Builder;

/**
 * Class Database
 * @package Ecommerce\Units\Library\Database
 */
class EloquentDatabase implements Contracts\EloquentDatabaseContract
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var string
     */
    protected $connection = 'default';

    /**
     * @var Manager
     */
    protected $resolver;

    /**
     * @var \Illuminate\Database\Schema\Builder
     */
    protected $schema;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        // $this->config();
    }

    /**
     * @return \Ecommerce\Units\Library\Database\EloquentDatabase
     */
    public function config(): EloquentDatabase
    {
        // Get config data
        $config = require __DIR__ . '/../../../../config/config.php';
        $this->config  = $config['db'];

        //dd($config['db']);

        // Resolver
        $this->resolver = new Manager();

        // Setup Database Connection and Eloquent
        $this->setupDatabase();

        // Schema
        $this->schema = $this->resolver::schema();

        return $this;
    }

    /**
     * @return void
     */
    protected function setupDatabase(): void
    {
        $connection = $this->resolver->getContainer()['config']['database.connections'];

        if ($connection === null) {
            $this->resolver->addConnection($this->config[$this->config['default']]);
        }

        // Set as Global
        $this->resolver->setAsGlobal();

        // Setup the Eloquent ORM…
        $this->resolver->bootEloquent();
    }

    /**
     * @return string
     */
    public function getConnection(): string
    {
        return $this->connection;
    }

    /**
     * @return Manager
     */
    public function getResolver(): Manager
    {
        return $this->resolver;
    }

    /**
     * @return \Illuminate\Database\Schema\Builder
     */
    public function getSchema(): Builder
    {
        return $this->schema;
    }

    /**
     * @return void
     */
    public function disconnect(): void
    {
        $this->resolver->getDatabaseManager()->disconnect('default');
    }
}
