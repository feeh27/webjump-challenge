<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database\Contracts;

use Ecommerce\Units\Library\Database\EloquentDatabase;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Builder;

/**
 * Interface DatabaseContract
 * @package Ecommerce\Units\Library\Database\Contracts
 */
interface EloquentDatabaseContract
{
    /**
     * @return string
     */
    public function getConnection(): string;

    /**
     * @return \Illuminate\Database\Capsule\Manager
     */
    public function getResolver(): Manager;

    /**
     * @return \Illuminate\Database\Schema\Builder
     */
    public function getSchema(): Builder;

    /**
     * @return \Ecommerce\Units\Library\Database\EloquentDatabase
     */
    public function config(): EloquentDatabase;
}
