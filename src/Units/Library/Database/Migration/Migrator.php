<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database\Migration;

/**
 * Class Migration
 * @package Ecommerce\Units\Library\Database\Migration
 */
class Migrator
{
    /**
     * @var array
     */
    protected $migrations = [];

    /**
     * @var \Ecommerce\Units\Library\Database\Migration\MigrationHandler
     */
    protected $migrationHandler;

    /**
     * EloquentMigration constructor.
     */
    public function __construct()
    {
        // Registra as classes disponíveis para migração
        $this->register();

        $this->migrationHandler = new MigrationHandler();
    }

    /**
     * @return void
     */
    protected function register(): void
    {
        $this->add(\Ecommerce\Domain\Categories\Database\Migration\CategoryMigration::class);
        $this->add(\Ecommerce\Domain\Products\Database\Migration\ProductMigration::class);
        $this->add(\Ecommerce\Domain\ProductCategory\Database\Migration\ProductCategoryMigration::class);
        $this->add(\Ecommerce\Domain\ProductImage\Database\Migration\ProductImageMigration::class);
    }

    /**
     * @param string $migrationClass
     * @return void
     */
    protected function add(string $migrationClass): void
    {
        // Adiciona as classes de migração em um array
        $this->migrations[] = $migrationClass;
    }

    /**
     * @return array
     */
    public function migrate(): array
    {
        return $this->migrationHandler->migrate($this->migrations);
    }

    /**
     * @return array
     */
    public function rollback(): array
    {
        return $this->migrationHandler->rollback();
    }

    /**
     * @return array
     */
    public function reset(): array
    {
        return $this->migrationHandler->reset();
    }
}
