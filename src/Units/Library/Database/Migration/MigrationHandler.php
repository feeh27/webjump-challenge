<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database\Migration;

/**
 * Class HandleMigrations
 * @package Ecommerce\Units\Library\Database\Migration
 */
class MigrationHandler
{
    /**
     * @var \Ecommerce\Units\Library\Database\Migration\MigrationRepository
     */
    protected $repository;

    /**
     * HandleEloquentMigrations constructor.
     */
    public function __construct()
    {
        $this->repository = new MigrationRepository();
    }

    /**
     * @param array $migrations
     * @return array
     */
    public function migrate(array $migrations): array
    {
        $migrated = [];

        // Pega o próximo batch a ser inserido
        $batch = $this->repository->getNextBatchNumber();

        // Faz uma iteração por todas as migrações
        foreach ($migrations as $migrationClass) {
            /**
             * @var \Ecommerce\Units\Library\Database\Migration\EloquentMigration $migration
             */
            $migration = new $migrationClass();

            if (! $this->repository->repositoryExists($migration->getTable())) {
                // Realiza a migração
                $migration->migrate();

                // Salva na tabela de migrações um registro auxiliar no rollback e nas próximas migrações
                $this->repository->log($migrationClass, $batch);

                $migrated[] = $migrationClass;
            }
        }

        return $migrated;
    }

    /**
     * @return array
     */
    public function rollback(): array
    {
        $migrated = [];

        // Pega todos os registros da última migração
        $migrations = $this->repository->getLast();

        // Faz uma iteração pelos registros da última migração
        foreach ($migrations as $migrationRegister) {
            $migrationClass = $migrationRegister->class;

            /**
             * @var \Ecommerce\Infrastructure\Domain\Migration\Contracts\MigrationContract $migration
             */
            $migration = new $migrationClass();

            // Realiza o rollback
            $migration->rollback();

            // Deleta o registro da tabela de migrações
            $this->repository->delete($migrationRegister);

            $migrated[] = $migrationClass;
        }

        return $migrated;
    }

    /**
     * @return array
     */
    public function reset(): array
    {
        $migrated = [];

        // Pega todas as migrações registradas
        $migrations = $this->repository->getAll();

        // Faz uma iteração pelos registros da última migração
        foreach ($migrations as $migrationRegister) {
            $migrationClass = $migrationRegister->class;

            /**
             * @var \Ecommerce\Infrastructure\Domain\Migration\Contracts\MigrationContract $migration
             */
            $migration = new $migrationClass();

            // Realiza o rollback
            $migration->rollback();

            // Deleta o registro da tabela de migrações
            $this->repository->delete($migrationRegister);

            $migrated[] = $migrationClass;
        }

        return $migrated;
    }
}