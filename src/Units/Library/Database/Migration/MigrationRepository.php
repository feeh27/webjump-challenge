<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database\Migration;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class MigrationRepository
 * @package Ecommerce\Units\Library\Database\Migration
 */
class MigrationRepository
{
    /**
     * @var string
     */
    protected $table = 'migrations';

    /**
     * @var string
     */
    protected $connection;

    /**
     * @var \Illuminate\Database\Capsule\Manager
     */
    protected $resolver;

    /**
     * @var \Illuminate\Database\Schema\Builder
     */
    protected $schema;

    /**
     * HandleEloquentMigrations constructor.
     */
    public function __construct()
    {
        $this->setup();

        // Cria a tabela de migrações caso ela não exista
        $this->createRepository();
    }

    /**
     * @return void
     */
    protected function setup(): void
    {
        $db = db();
        $db->config();
        $this->resolver   = $db->getResolver();
        $this->schema     = $db->getSchema();
        $this->connection = $db->getConnection();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $query = $this->table();

        $result = $query->orderBy('class', 'DESC')->get();

        return $result->all();
    }

    /**
     * @return array
     */
    public function getLast(): array
    {
        $query = $this->table()->where('batch', $this->getLastBatchNumber());

        $result = $query->orderBy('class', 'DESC')->get();

        return $result->all();
    }

    /**
     * @param string $file
     * @param int $batch
     */
    public function log(string $file, int $batch): void
    {
        $record = ['class' => $file, 'batch' => $batch];

        $this->table()->insert($record);
    }

    /**
     * @param $migration
     */
    public function delete($migration): void
    {
        $this->table()->where('class', $migration->class)->delete();
    }

    /**
     * @return int
     */
    public function getNextBatchNumber(): int
    {
        return $this->getLastBatchNumber() + 1;
    }

    /**
     * @return int|null
     */
    public function getLastBatchNumber(): ?int
    {
        return $this->table()->max('batch');
    }

    /**
     * Cria a tabela de migrações
     */
    public function createRepository(): void
    {
        if (! $this->repositoryExists($this->table)) {
            $this->schema->create($this->table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('class');
                $table->unsignedTinyInteger('batch');
            });
        }
    }

    /**
     * @param string $table
     * @return bool
     */
    public function repositoryExists(string $table): bool
    {
        $schema = $this->getConnection()->getSchemaBuilder();
        return $schema->hasTable($table);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    protected function table(): QueryBuilder
    {
        return $this->getConnection()->table($this->table);
    }

    /**
     * Resolve the database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    public function getConnection(): Connection
    {
        return $this->resolver->connection($this->connection);
    }
}
