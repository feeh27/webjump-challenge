<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database\Migration;

use Illuminate\Database\Migrations\Migration;

/**
 * Class EloquentMigration
 * @package Ecommerce\Units\Library\Database\Migration
 */
class EloquentMigration extends Migration
{
    /**
     * @var \Illuminate\Database\Schema\Builder
     */
    protected $schema;

    /**
     * @var string
     */
    protected $table = '';

    /**
     * EloquentMigration constructor.
     */
    public function __construct()
    {
        $db = db();
        $db->config();
        $this->schema = $db->getSchema();
        $this->connection = $db->getConnection();
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }
}