<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Database;

use Illuminate\Database\ConnectionResolver;

/**
 * Class EloquentDatabaseResolver
 * @package Ecommerce\Units\Library\Database
 */
class EloquentDatabaseResolver extends ConnectionResolver
{
    public function __construct()
    {
        $this->config();
        $connections = $this->getConnections();
        parent::__construct($connections);
    }

    /**
     * @return void
     */
    private function config(): void
    {
        $this->default = 'default';
    }

    /**
     * @return array
     */
    private function getConnections(): array
    {
        $db = db()->config();

        $connections = [];
        $connections['default'] = $db->getResolver()->getConnection('default');

        return $connections;
    }
}
