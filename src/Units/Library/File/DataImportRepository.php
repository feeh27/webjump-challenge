<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File;

use Ecommerce\Units\Library\Log\Logger;
use Exception;

/**
 * Class DataImportRepository
 * @package Ecommerce\Units\Library\File
 */
class DataImportRepository
{
    /**
     * @var null|\Illuminate\Database\Connection
     */
    protected $connection = null;

    /**
     * @var null|\Ecommerce\Units\Library\Database\EloquentDatabase
     */
    protected $database = null;

    /**
     * DataImportRepository constructor.
     */
    public function __construct()
    {
        $this->database = db();

        $this->database->config();

        $this->connection = $this->database->getResolver()->getConnection();
    }

    /**
     * DataImportRepository destructor
     */
    public function __destruct()
    {
        $this->database->disconnect();
    }

    /**
     * @param string $model
     * @param callable $callback
     * @return bool
     * @throws Exception
     */
    public function process(string $model, callable $callback): bool
    {
        $this->connection->beginTransaction();

        $success = false;

        try {
            $callback();
            $this->connection->commit();
            $success = true;
        } catch (Exception $e) {
            $logger = new Logger();
            $logger->logImportException($model, self::class, $e);
            $this->connection->rollBack();
        }

        return $success;
    }

}