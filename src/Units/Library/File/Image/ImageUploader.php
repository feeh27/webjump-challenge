<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File\Image;

use Ecommerce\Units\Exceptions\Http\ValidationException;
use Pecee\Http\Input\InputFile;

/**
 * Class ImageUploader
 * @package Ecommerce\Units\Library\File\Image
 */
class ImageUploader
{
    /**
     * @var array
     */
    protected $data;

    /**
     * ImageUploader constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param InputFile $image
     * @param string $model
     * @param int $id
     * @return string
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function imageToUrl(string $model, $id): string
    {
        $image = $this->validateImage();

        // Directory file
        $path = images_path() . '/' . strtolower($model) . '/' . $id;

        // Image name
        $imageName = sprintf('%s.%s', uniqid(), $image->getExtension());

        // Full image path
        $destination = sprintf('%s/%s', $this->createImagePath($path), $imageName);

        if (! $this->process($image, $destination)) {
            return '';
        }

        $relative_path = str_replace('/var/www/public', '', $destination);

        return $relative_path;
    }

    /**
     * @param InputFile $image
     * @param string $destination
     * @return bool
     */
    protected function process(InputFile $image, string $destination): bool
    {
        $status = false;

        $image->move($destination);

        if (file_exists($destination)) {
            $status = true;
        }

        return $status;
    }

    /**
     * Create a photo directory, if not exists
     *
     * @param string $path
     * @return string
     */
    protected function createImagePath(string $path): string
    {
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        return $path;
    }

    /**
     * @return \Pecee\Http\Input\InputFile
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    protected function validateImage(): InputFile
    {
        $validation = [];
        $validation['data'] = $this->data;

        if ((! isset($this->data['image'])) || (! $this->data['image'] instanceof InputFile)) {
            $message = 'This field not be empty and only accepts the following extensions: png, jpeg and gif images';
            $validation['validation']['image'][] = $message;
            throw new ValidationException(json_encode($validation));
        } else {
            $image = $this->data['image'];
        }

        $type = $image->getType();
        $size = $image->getSize();

        // File types accept
        if (! in_array($type, ['image/png', 'image/jpeg', 'image/gif'])) {
            $message = 'This field only accepts the following extensions: png, jpeg and gif images';
            $validation['validation']['image'][] = $message;
            throw new ValidationException(json_encode($validation));
        }

        // Max file size is 5M
        if ($size > 5000000) {
            $message = 'File is too large, the max file size is 5MB!';
            $validation['validation']['image'][] = $message;
            throw new ValidationException(json_encode($validation));
        }

        return $image;
    }
}
