<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File\Csv;

use Closure;
use Ecommerce\Infrastructure\Domain\File\Csv\Contracts\CsvExternalMapContract;

/**
 * Class ExternalCsvMap
 * @package Ecommerce\Infrastructure\Domain\File\Csv
 */
class CsvExternalMap extends CsvMap implements CsvExternalMapContract
{
    /**
     * @var string
     */
    protected $delimiter = '';

    /**
     * CsvExternalMap constructor.
     * @param string $csvField
     * @param string $delimiter
     */
    public function __construct(string $csvField, string $delimiter = '')
    {
        $this->delimiter = $delimiter;
        parent::__construct($csvField);
    }

    /**
     * {@inheritDoc}
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * {@inheritDoc}
     */
    public function setDelimiter(string $delimiter): void
    {
        $this->delimiter = $delimiter;
    }
}
