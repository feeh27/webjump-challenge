<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File\Csv;

use Ecommerce\Infrastructure\Domain\File\Csv\Contracts\ModelCsvHandlerContract;
use League\Csv\Reader;
use League\Csv\Statement;

/**
 * Class CsvHandler
 * @package Ecommerce\Units\Library\File\Csv
 */
class CsvHandler
{
    /**
     * @var \Ecommerce\Infrastructure\Domain\File\Csv\Contracts\ModelCsvHandlerContract
     */
    protected $modelCsvHandler;

    /**
     * @var \League\Csv\AbstractCsv
     */
    protected $csv;

    /**
     * CsvHandler constructor.
     * @param ModelCsvHandlerContract $modelCsvHandler
     * @param string $openMode
     * @throws \League\Csv\Exception
     */
    public function __construct(ModelCsvHandlerContract $modelCsvHandler, string $openMode)
    {
        $this->modelCsvHandler = $modelCsvHandler;
        $filepath = assets_path($modelCsvHandler->getFilepath());
        $this->csv = Reader::createFromPath($filepath, $openMode);
        $this->config();
    }

    /**
     * @return void
     * @throws \League\Csv\Exception
     */
    protected function config(): void
    {
        $delimiter = $this->modelCsvHandler->getDelimiter();
        $this->csv->setDelimiter($delimiter);
        if ($this->csv instanceof Reader) {
            $this->csv->setHeaderOffset(0);
        }
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @return \Ecommerce\Units\Library\File\Csv\CsvProcessResult
     * @throws \League\Csv\Exception
     */
    public function process(int $offset = null, int $limit = null): CsvProcessResult
    {
        $counter = 0;
        $rows = 0;
        $currentRow = 1;

        if ($this->csv instanceof Reader) {
            $stmt = $this->prepareProcess($offset, $limit);

            $records = $stmt->process($this->csv);

            $rows = count($records);

            foreach ($records as $record) {
                if ($this->modelCsvHandler->process($record)) {
                    $counter++;
                }

                $currentRow++;
            }
        }

        $processResult = new CsvProcessResult($rows, $counter);

        return $processResult;
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @return \League\Csv\Statement
     * @throws \League\Csv\Exception
     */
    protected function prepareProcess(int $offset = null, int $limit = null)
    {
        $stmt = new Statement();

        if ($offset !== null) {
            $stmt = $stmt->offset($offset);
        }

        if ($limit !== null) {
            $stmt = $stmt->limit($limit);
        }

        return $stmt;
    }
}
