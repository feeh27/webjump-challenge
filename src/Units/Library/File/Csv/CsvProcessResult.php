<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File\Csv;

/**
 * Class CsvProcessResult
 * @package Ecommerce\Units\Library\File\Csv
 */
class CsvProcessResult
{
    /**
     * @var int
     */
    protected $rows;

    /**
     * @var int
     */
    protected $registers;

    /**
     * CsvProcessResult constructor.
     * @param int $rows
     * @param int $registers
     */
    public function __construct(int $rows, int $registers)
    {
        $this->rows = $rows;
        $this->registers = $registers;
    }

    /**
     * @return int
     */
    public function getRows(): int
    {
        return $this->rows;
    }

    /**
     * @param int $rows
     */
    public function setRows(int $rows): void
    {
        $this->rows = $rows;
    }

    /**
     * @return int
     */
    public function getRegisters(): int
    {
        return $this->registers;
    }

    /**
     * @param int $registers
     */
    public function setRegisters(int $registers): void
    {
        $this->registers = $registers;
    }
}
