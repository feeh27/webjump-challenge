<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File\Csv;

use Ecommerce\Infrastructure\Domain\File\Csv\Contracts\CsvDbMapContract;

/**
 * Class CsvMap
 * @package Ecommerce\Infrastructure\Domain\File\Csv
 */
class CsvDbMap extends CsvMap implements CsvDbMapContract
{
    /**
     * @var string
     */
    protected $dbField;

    /**
     * CsvMap constructor.
     * @param string $csvField
     * @param string $dbField
     */
    public function __construct(string $csvField, string $dbField )
    {
        parent::__construct($csvField);
        $this->dbField = $dbField;
    }

    /**
     * {@inheritDoc}
     */
    public function getDbField(): string
    {
        return $this->dbField;
    }

    /**
     * {@inheritDoc}
     */
    public function setDbField(string $dbField): void
    {
        $this->dbField = $dbField;
    }
}
