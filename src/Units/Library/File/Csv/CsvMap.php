<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\File\Csv;

use Ecommerce\Infrastructure\Domain\File\Csv\Contracts\CsvMapContract;

/**
 * Class CsvMap
 * @package Ecommerce\Infrastructure\Domain\File\Csv
 */
class CsvMap implements CsvMapContract
{
    /**
     * @var string
     */
    protected $csvField;

    /**
     * CsvMap constructor.
     * @param string $csvField
     */
    public function __construct(string $csvField)
    {
        $this->csvField = $csvField;
    }

    /**
     * {@inheritDoc}
     */
    public function getCsvField(): string
    {
        return $this->csvField;
    }

    /**
     * {@inheritDoc}
     */
    public function setCsvField(string $csvField): void
    {
        $this->csvField = $csvField;
    }
}
