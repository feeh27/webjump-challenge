<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Http\Response;

/**
 * Class HttpCode
 * @package Ecommerce\Units\Library\Http\Response
 */
class HttpCode
{
    // Success response
    const HTTP_STATUS_OK = 200;
    const HTTP_STATUS_CREATED = 201;
    const HTTP_STATUS_NO_CONTENT = 204;

    // Redirect response
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_PERMANENT_REDIRECT = 308;

    // Client error
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_STATUS_NOT_FOUND = 404;
    const HTTP_STATUS_METHOD_NOT_ALLOWED = 405;
    const HTTP_STATUS_UNPROCESSABLE_ENTITY = 422;

    // Server error
    const HTTP_STATUS_INTERNAL_SERVER_ERROR = 500;
    const HTTP_STATUS_SERVICE_UNAVAILABLE = 503;

    const INFO_CODES = [
        '100',
        '101',
        '102',
    ];

    const SUCCESS_CODES = [
        '200',
        '201',
        '202',
        '203',
        '204',
        '205',
        '206',
        '207',
        '208',
        '226',
    ];

    const REDIRECT_CODES = [
        '300',
        '301',
        '302',
        '303',
        '304',
        '305',
        '306',
        '307',
        '308',
    ];

    const CLIENT_ERROR_CODES = [
        '400',
        '401',
        '402',
        '403',
        '404',
        '405',
        '406',
        '407',
        '408',
        '409',
        '410',
        '411',
        '412',
        '413',
        '414',
        '415',
        '416',
        '417',
        '418',
        '422',
        '423',
        '424',
        '426',
        '428',
        '429',
        '431',
        '451',
    ];

    const SERVER_ERROR_CODES = [
        '500',
        '501',
        '502',
        '503',
        '504',
        '505',
        '506',
        '507',
        '508',
        '510',
        '511',
    ];

    /**
     * @var array
     */
    const MESSAGES = [
        200 => 'OK',
        201 => 'CREATED',
        204 => 'NO CONTENT',
        301 => 'MOVED PERMANENTLY',
        307 => 'TEMPORARY REDIRECT',
        308 => 'PERMANENT REDIRECT',
        401 => 'UNAUTHORIZED',
        403 => 'FORBIDDEN',
        404 => 'NOT FOUND',
        405 => 'METHOD NOT ',
        422 => 'UNPROCESSABLE_ENTITY',
        500 => 'INTERNAL_SERVER_ERROR',
        503 => 'SERVICE_UNAVAILABLE',
    ];

    /**
     * @param int $httpCode
     * @return bool
     */
    public static function isSuccess(int $httpCode): bool
    {
        return in_array($httpCode, self::SUCCESS_CODES);
    }

    /**
     * @param int $httpCode
     * @return bool
     */
    public static function isClientError(int $httpCode): bool
    {
        return in_array($httpCode, self::CLIENT_ERROR_CODES);
    }

    /**
     * @param int $httpCode
     * @return bool
     */
    public static function isServerError(int $httpCode): bool
    {
        return in_array($httpCode, self::SERVER_ERROR_CODES);
    }

    /**
     * @param int $httpCode
     * @return string
     */
    public function getMessage(int $httpCode): string
    {
        $messages = self::MESSAGES;

        if (isset($messages[$httpCode])) {
            return $messages[$httpCode];
        }

        return '';
    }
}
