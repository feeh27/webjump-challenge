<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Http\Middleware;

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

/**
 * Class WebMiddleware
 * @package Ecommerce\Units\Library\Http\Middlewares
 */
class WebMiddleware implements IMiddleware
{
    /**
     * {@inheritDoc}
     */
    public function handle(Request $request): void
    {
        session()->start();
    }

    /**
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function __destruct()
    {
        session()->handleDestruct();
    }
}