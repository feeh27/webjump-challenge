<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Http\Middleware;

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

/**
 * Class ApiMiddleware
 * @package Ecommerce\Units\Library\Http\Middlewares
 */
class ApiMiddleware implements IMiddleware
{
    public function handle(Request $request): void
    {
        //dd($request->getHeaders());
    }
}