<?php

declare(strict_types=1);

namespace Ecommerce\Units\Library\Http\Route;

use Closure;
use Pecee\SimpleRouter\SimpleRouter;

/**
 * Class Router
 * @package Ecommerce\Units\Library\Http\Router
 */
class Router
{
    /**
     * @var \Pecee\SimpleRouter\SimpleRouter
     */
    protected $router;

    /**
     * @var \Ecommerce\Units\Library\Database\EloquentDatabase
     */
    protected $database;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->router = new SimpleRouter();
    }

    /**
     * @throws \Pecee\Http\Middleware\Exceptions\TokenMismatchException
     * @throws \Pecee\SimpleRouter\Exceptions\HttpException
     * @throws \Pecee\SimpleRouter\Exceptions\NotFoundHttpException
     */
    public static function start(): void
    {
        if (request()->getUrl()->contains('/api')) {
            // Load custom api routes
            require_once __DIR__ . '/../../../../../routes/api.php';
        } else {
            // Load custom web routes
            require_once __DIR__ . '/../../../../../routes/web.php';
        }

        SimpleRouter::start();
    }

    /**
     * @param string $url
     * @param Closure|string $callback
     * @param array|void $settings
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function get(string $url, $callback, array $settings = [])
    {
        return SimpleRouter::get($url, $callback, $settings);
    }

    /**
     * @param string $url
     * @param Closure|string $callback
     * @param array|void $settings
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function post(string $url, $callback, array $settings = [])
    {
        return SimpleRouter::post($url, $callback, $settings);
    }

    /**
     * @param string $url
     * @param Closure|string $callback
     * @param array|void $settings
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function put(string $url, $callback, array $settings = [])
    {
        return SimpleRouter::put($url, $callback, $settings);
    }

    /**
     * @param string $url
     * @param Closure|string $callback
     * @param array|void $settings
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function patch(string $url, $callback, array $settings = [])
    {
        return SimpleRouter::patch($url, $callback, $settings);
    }

    /**
     * @param string $url
     * @param Closure|string $callback
     * @param array|void $settings
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function delete(string $url, $callback, array $settings = [])
    {
        return SimpleRouter::delete($url, $callback, $settings);
    }

    /**
     * @param string $url
     * @param Closure|string $callback
     * @param array|void $settings
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function options(string $url, $callback, array $settings = [])
    {
        return SimpleRouter::options($url, $callback, $settings);
    }

    /**
     * @param array $settings
     * @param Closure|null $callback
     * @return \Pecee\SimpleRouter\Route\IRoute|\Pecee\SimpleRouter\Route\RouteUrl
     */
    public function group(array $settings, Closure $callback)
    {
        return SimpleRouter::group($settings, $callback);
    }
}
