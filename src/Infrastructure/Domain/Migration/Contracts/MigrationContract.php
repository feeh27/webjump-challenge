<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Migration\Contracts;

/**
 * Interface MigrationContract
 * @package Ecommerce\Infrastructure\Domain\Migration\Contracts
 */
interface MigrationContract
{
    /**
     * @return void
     */
    public function migrate(): void;

    /**
     * @return void
     */
    public function rollback(): void;
}