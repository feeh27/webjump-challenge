<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Migration;

/**
 * Class Migration
 * @package Ecommerce\Infrastructure\Domain\Migration
 */
abstract class Migration implements Contracts\MigrationContract
{
    /**
     * {@inheritDoc}
     */
    abstract public function migrate(): void;

    /**
     * {@inheritDoc}
     */
    abstract public function rollback(): void;
}