<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Validation\Contracts;

/**
 * Interface ValidationContract
 * @package Ecommerce\Infrastructure\Domain\Validation\Contracts
 */
interface ValidationContract
{
    /**
     * @param array $data
     * @return array
     */
    public function getCreateValidation(array $data): array;

    /**
     * @param array $data
     * @return array
     */
    public function getUpdateValidation(array $data): array;

    /**
     * @return array
     */
    public function getMessages(): array;

    /**
     * @return array
     */
    public function getCustomAttr(): array;
}