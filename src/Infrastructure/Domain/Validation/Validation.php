<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Validation;

use Ecommerce\Infrastructure\Core\BaseValidation;

/**
 * Class Validation
 * @package Ecommerce\Infrastructure\Domain\Validation
 */
abstract class Validation extends BaseValidation implements Contracts\ValidationContract
{
    /**
     * @param array $data
     * @return array
     */
    abstract public function getCreateValidation(array $data): array;

    /**
     * @param array $data
     * @return array
     */
    abstract public function getFirstOrCreateValidation(array $data): array;

    /**
     * @param array $data
     * @return array
     */
    abstract public function getUpdateValidation(array $data): array;

    /**
     * @return array
     */
    abstract public function getMessages(): array;

    /**
     * @return array
     */
    abstract public function getCustomAttr(): array;
}