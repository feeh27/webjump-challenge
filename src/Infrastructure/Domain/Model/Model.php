<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Model;

use Ecommerce\Infrastructure\Core\BaseModel;

/**
 * Class Model
 * @package Ecommerce\Infrastructure\Domain
 *
 * @method static firstOrCreate(array $attributes, array $values = []): self
 * @method static create(array $attributes): self
 * @method static insert(array $data)
 */
abstract class Model extends BaseModel implements Contracts\ModelContract
{
    /**
     * @var array
     */
    protected $enabled_filters = [
        'id'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return array
     */
    public function getEnabledFilters(): array
    {
        return $this->enabled_filters;
    }
}