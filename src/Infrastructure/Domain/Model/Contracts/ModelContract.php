<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Model\Contracts;

/**
 * Interface ModelContract
 * @package Ecommerce\Infrastructure\Domain\Model\Contracts
 */
interface ModelContract
{
    /**
     * @return array
     */
    public function getEnabledFilters(): array;
}