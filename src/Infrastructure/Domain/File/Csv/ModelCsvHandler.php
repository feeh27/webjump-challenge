<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\File\Csv;

use Ecommerce\Infrastructure\Core\BaseCsvHandler;
use Ecommerce\Infrastructure\Domain\Service\Service;
use Ecommerce\Units\Library\File\Csv\CsvDbMap;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CsvHandler
 * @package Ecommerce\Infrastructure\Domain\File
 */
abstract class ModelCsvHandler extends BaseCsvHandler implements Contracts\ModelCsvHandlerContract
{
    /**
     * @var string
     */
    protected $serviceClass = Service::class;

    /**
     * @var string
     */
    protected $filepath = '';

    /**
     * @var string
     */
    protected $delimiter = ';';

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * // Campo do CSV => nome do campo
     * @var array
     */
    protected $map = [];

    /**
     * // Campo do CSV => nome da funcao
     * @var array
     */
    protected $externalMap = [];

    /**
     * @return \Ecommerce\Infrastructure\Domain\Service\Service
     */
    protected function newService(): Service
    {
        return new $this->serviceClass;
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return $this->filepath;
    }

    /**
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @return array
     */
    public function getExternalMap(): array
    {
        return $this->externalMap;
    }

    /**
     * @param array $record
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    protected function addFromRecord(array $record): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\CrudService $service
         */
        $service = $this->newService();

        $data = $this->map($record);

        return $service->create($data);
    }

    /**
     * @param array $record
     * @return array
     */
    protected function map(array $record): array
    {
        $mapped = [];

        foreach ($this->map as $map) {
            if ($map instanceof CsvDbMap) {
                $data = $record[$map->getCsvField()];

                $mapped[$map->getDbField()] = $data;
            }
        }

        return $mapped;
    }

    /**
     * @return void
     */
    abstract protected function populateMap(): void;

    /**
     * @return void
     */
    abstract protected function populateExternalMap(): void;

    /**
     * {@inheritDoc}
     */
    abstract public function process(array $record): bool;
}
