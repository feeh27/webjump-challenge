<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\File\Csv\Contracts;

/**
 * Interface CsvHandlerContract
 * @package Ecommerce\Infrastructure\Domain\File\Contracts
 */
interface ModelCsvHandlerContract
{
    /**
     * @return string
     */
    public function getFilepath(): string;

    /**
     * @return string
     */
    public function getDelimiter(): string;

    /**
     * @return array
     */
    public function getFields(): array;

    /**
     * @return array
     */
    public function getMap(): array;

    /**
     * @return array
     */
    public function getExternalMap(): array;

    /**
     * @param array $record
     * @return bool
     */
    public function process(array $record): bool;
}