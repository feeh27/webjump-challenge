<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\File\Csv\Contracts;

/**
 * Interface CsvMapContract
 * @package Ecommerce\Infrastructure\Domain\File\Csv\Contracts
 */
interface CsvMapContract
{
    /**
     * @return string
     */
    public function getCsvField(): string;

    /**
     * @param string $csvField
     */
    public function setCsvField(string $csvField): void;
}
