<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\File\Csv\Contracts;

/**
 * Interface CsvDbMapContract
 * @package Ecommerce\Infrastructure\Domain\File\Csv\Contracts
 */
interface CsvDbMapContract extends CsvMapContract
{
    /**
     * @return string
     */
    public function getDbField(): string;

    /**
     * @param string $dbField
     */
    public function setDbField(string $dbField): void;
}