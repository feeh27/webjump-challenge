<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\File\Csv\Contracts;

use Closure;

/**
 * Interface CsvExternalMapContract
 * @package Ecommerce\Infrastructure\Domain\File\Csv\Contracts
 */
interface CsvExternalMapContract extends CsvMapContract
{
    /**
     * @return string
     */
    public function getDelimiter(): string;

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter): void;
}