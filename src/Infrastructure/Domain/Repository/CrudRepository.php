<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Repository;

use Ecommerce\Units\Library\Log\Logger;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package Ecommerce\Infrastructure\Domain
 */
abstract class CrudRepository extends Repository implements Contracts\CrudRepositoryContract
{
    /**
     * {@inheritDoc}
     */
    public function get()
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        $repository->setFilters($this->filters);

        return $repository->get();
    }

    /**
     * {@inheritDoc}
     */
    public function getById($id): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        return $repository->getById($id);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        $logMessage = 'Create register';
        $logData = [
            'data' => $data,
        ];
        $logger = new Logger();
        $logger->logDbActions($logMessage, $this->modelClass, 'CREATE', $logData);

        return $repository->create($data);
    }

    /**
     * {@inheritDoc}
     */
    public function firstOrCreate($data): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        $logMessage = 'Create register';
        $logData = [
            'data' => $data,
        ];
        $logger = new Logger();
        $logger->logDbActions($logMessage, $this->modelClass, 'CREATE', $logData);

        return $repository->firstOrCreate($data);
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $data): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        $logMessage = 'Update register';
        $logData = [
            'id' => $id,
            'data' => $data,
        ];
        $logger = new Logger();
        $logger->logDbActions($logMessage, $this->modelClass, 'UPDATE', $logData);

        return $repository->update($id, $data);
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id): bool
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        $logMessage = 'Delete register';
        $logData = [
            'id' => $id,
        ];
        $logger = new Logger();
        $logger->logDbActions($logMessage, $this->modelClass, 'DELETE', $logData);

        return $repository->delete($id);
    }

    /**
     * {@inheritDoc}
     */
    public function restore($id): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\Contracts\CrudRepositoryContract $repository
         */
        $repository = $this->newRepository();

        $logMessage = 'Restore register';
        $logData = [
            'id' => $id,
        ];
        $logger = new Logger();
        $logger->logDbActions($logMessage, $this->modelClass, 'RESTORE', $logData);

        return $repository->restore($id);
    }
}
