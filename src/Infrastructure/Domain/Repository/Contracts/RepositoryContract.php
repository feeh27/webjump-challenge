<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Repository\Contracts;

/**
 * Interface RepositoryContract
 * @package Ecommerce\Infrastructure\Domain\Repository\Contracts
 */
interface RepositoryContract
{
    /**
     * @param array $filters
     * @return void
     */
    public function setFilters(array $filters);
}