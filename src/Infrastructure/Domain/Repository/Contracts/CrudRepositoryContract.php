<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Repository\Contracts;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryContract
 * @package Ecommerce\Infrastructure\Domain\Repository\Contracts
 */
interface CrudRepositoryContract extends RepositoryContract
{
    /**
     * @return \Illuminate\Support\Collection|array
     */
    public function get();

    /**
     * @param int|string $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getById($id): Model;

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data): Model;

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function firstOrCreate(array $data): Model;

    /**
     * @param int|string $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data): Model;

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id): bool;

    /**
     * @param int|string $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function restore($id): Model;
}