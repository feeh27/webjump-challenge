<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Repository;

use Ecommerce\Infrastructure\Core\BaseRepository;
use Ecommerce\Infrastructure\Domain\Model\Model;

/**
 * Class Repository
 * @package Ecommerce\Infrastructure\Domain
 */
abstract class Repository extends BaseRepository implements Contracts\RepositoryContract
{
    /**
     * @var string
     */
    protected $connection = 'default';

    /**
     * {@inheritDoc}
     */
    protected $modelClass = Model::class;

    /**
     * @var string
     */
    protected $repositoryClass = BaseRepository::class;

    /**
     * Cria uma instância de Repository
     *
     * @return \Ecommerce\Infrastructure\Core\BaseRepository
     */
    protected function newRepository(): BaseRepository
    {
        $connection = $this->connection;
        $modelClass = $this->modelClass;

        return new $this->repositoryClass($modelClass, $connection);
    }
}