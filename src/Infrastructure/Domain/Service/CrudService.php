<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Service;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Service
 * @package Ecommerce\Infrastructure\Domain
 */
abstract class CrudService extends Service implements Contracts\CrudServiceContract
{
    /**
     * {@inheritDoc}
     */
    public function get()
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        $repository->setFilters($this->filters);

        return $repository->get();
    }

    /**
     * {@inheritDoc}
     */
    public function getById($id): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        return $repository->getById($id);
    }

    /**
     * {@inheritDoc}
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function create(array $data): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Validation\Contracts\ValidationContract $validation
         */
        $validation = $this->newValidation();

        $rules = $validation->getCreateValidation($data);
        $messages = $validation->getMessages();
        $customAttr = $validation->getCustomAttr();

        $this->validator->validate($data, $rules, $messages, $customAttr);

        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        return $repository->create($data);
    }

    /**
     * {@inheritDoc}
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function firstOrCreate(array $data): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Validation\Contracts\ValidationContract $validation
         */
        $validation = $this->newValidation();

        $rules = $validation->getFirstOrCreateValidation($data);
        $messages = $validation->getMessages();
        $customAttr = $validation->getCustomAttr();

        $this->validator->validate($data, $rules, $messages, $customAttr);

        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        return $repository->firstOrCreate($data);
    }

    /**
     * {@inheritDoc}
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function update($id, array $data): Model
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Validation\Contracts\ValidationContract $validation
         */
        $validation = $this->newValidation();

        $validationData = $data;
        $validationData['id'] = $id;

        $rules = $validation->getUpdateValidation($validationData);
        $messages = $validation->getMessages();
        $customAttr = $validation->getCustomAttr();

        $this->validator->validate($data, $rules, $messages, $customAttr);

        /**
         * @var \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        return $repository->update($id, $data);
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id): bool
    {
        /**
         * @return \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        return $repository->delete($id);
    }

    /**
     * {@inheritDoc}
     */
    public function restore($id): Model
    {
        /**
         * @return \Ecommerce\Infrastructure\Domain\Repository\CrudRepository $repository
         */
        $repository = $this->newRepository();

        return $repository->restore($id);
    }
}
