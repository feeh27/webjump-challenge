<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Service;

use Ecommerce\Infrastructure\Core\BaseService;
use Ecommerce\Infrastructure\Domain\Repository\Repository;
use Ecommerce\Infrastructure\Domain\Validation\Validation;
use Ecommerce\Units\Library\Validation\Validator;

/**
 * Class Service
 * @package Ecommerce\Infrastructure\Domain
 */
abstract class Service extends BaseService implements Contracts\ServiceContract
{
    /**
     * {@inheritDoc}
     */
    protected $repositoryClass = Repository::class;

    /**
     * {@inheritDoc}
     */
    protected $validationClass = Validation::class;

    /**
     * @var Validator
     */
    protected $validator = null;

    /**
     * Service constructor.
     */
    public function __construct()
    {
        $this->validator = new Validator();
    }
}