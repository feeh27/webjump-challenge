<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Service\Contracts;

/**
 * Interface ServiceContract
 * @package Ecommerce\Infrastructure\Domain\Service\Contracts
 */
interface ServiceContract
{
    /**
     * @param array $filters
     * @return mixed
     */
    public function setFilters(array $filters): void;
}