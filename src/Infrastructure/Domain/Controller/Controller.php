<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Controller;

use Ecommerce\Infrastructure\Core\BaseController;
use Ecommerce\Infrastructure\Domain\Service\Service;

/**
 * Class Controller
 * @package Ecommerce\Infrastructure\Domain
 */
abstract class Controller extends BaseController implements Contracts\ControllerContract
{
    /**
     * {@inheritDoc}
     */
    protected $serviceClass = Service::class;
}