<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Controller;

/**
 * Class Controller
 * @package Ecommerce\Infrastructure\Domain
 */
abstract class CrudController extends Controller implements Contracts\CrudControllerContract
{
    /**
     * {@inheritDoc}
     */
    public function get()
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $query = url()->getParams();

        $service->setFilters($query);

        $content = $service->get()->toArray();

        $response = [
            'status'  => SUCCESS,
            'message' => 'Dados recebidos com sucesso!',
        ];

        if (array_key_exists('pagination', $content)) {
            $response['pagination'] = $content['pagination'];
        }

        if (array_key_exists('data', $content)) {
            $response['data'] = $content['data'];
        }

        response()->httpCode(200)->json($response);
    }

    /**
     * {@inheritDoc}
     */
    public function getById($id)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $response = [
            'status'  => SUCCESS,
            'message' => 'Dados recebidos com sucesso!',
            'data'    => $service->getById($id),
        ];

        response()->httpCode(200)->json($response);
    }

    /**
     * {@inheritDoc}
     */
    public function create()
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $data = [];

        $response = [
            'status'  => SUCCESS,
            'message' => 'Registro criado com sucesso!',
            'data'    => $service->create($data),
        ];

        response()->httpCode(201)->json($response);
    }

    /**
     * {@inheritDoc}
     */
    public function firstOrCreate()
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $data = [];

        $response = [
            'status'  => SUCCESS,
            'message' => 'Registro criado com sucesso!',
            'data'    => $service->firstOrCreate($data),
        ];

        response()->httpCode(201)->json($response);
    }

    /**
     * {@inheritDoc}
     */
    public function update($id)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $data = [];

        $response = [
            'status'  => SUCCESS,
            'message' => 'Registros atualizados com sucesso!',
            'data'    => $service->update($id, $data),
        ];

        response()->httpCode(200)->json($response);
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        if ($service->delete($id)) {
            response()->httpCode(204);
        } else {
            $response = [
                'status'  => ERROR,
                'message' => 'Falha ao deletar registro!',
                'data'    => [],
            ];

            response()->httpCode(404)->json($response);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function restore($id)
    {
        /**
         * @var \Ecommerce\Infrastructure\Domain\Service\Contracts\CrudServiceContract $service
         */
        $service = $this->newService();

        $response = [
            'status'  => SUCCESS,
            'message' => 'Registro restaurado com sucesso!',
            'data'    => $service->restore($id),
        ];

        response()->httpCode(200)->json($response);
    }
}