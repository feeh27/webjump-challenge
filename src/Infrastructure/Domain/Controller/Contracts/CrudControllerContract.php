<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Controller\Contracts;

/**
 * Interface ControllerContract
 * @package Ecommerce\Infrastructure\Domain\Controller\Contracts
 */
interface CrudControllerContract extends ControllerContract
{
    /**
     * @return mixed|void
     */
    public function get();

    /**
     * @param int|string $id
     * @return mixed|void
     */
    public function getById($id);

    /**
     * @return mixed|void
     */
    public function create();

    /**
     * @return mixed|void
     */
    public function firstOrCreate();

    /**
     * @param int|string $id
     * @return mixed|void
     */
    public function update($id);

    /**
     * @param int|string $id
     * @return mixed|void
     */
    public function delete($id);

    /**
     * @param int|string $id
     * @return mixed|void
     */
    public function restore($id);
}
