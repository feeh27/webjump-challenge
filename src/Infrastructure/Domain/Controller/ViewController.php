<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Domain\Controller;

use Ecommerce\Infrastructure\Core\BaseController;
use Ecommerce\Infrastructure\Domain\Service\Service;
use Ecommerce\Units\Library\Render\Template;

/**
 * Class ViewController
 * @package Ecommerce\Infrastructure\Domain\Controller
 */
abstract class ViewController extends BaseController implements Contracts\ControllerContract
{
    /**
     * {@inheritDoc}
     */
    protected $serviceClass = Service::class;

    /**
     * @var \Ecommerce\Units\Library\Render\Template $template
     */
    protected $template;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->template = new Template();
    }

    /**
     * @param array $data
     * @return array
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    protected function getFlashData(array $data): array
    {
        $data = $this->getValidFlashData($data, 'message');

        $data = $this->getValidFlashData($data, 'validation');

        $data = $this->getValidFlashData($data, 'data');

        return $data;
    }

    /**
     * @param array $data
     * @param string $name
     * @return array
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    protected function getValidFlashData(array $data, string $name): array
    {
        $flashData = session()->readFlash($name);

        if ($flashData !== null && ! empty($flashData)) {
            $data[$name] = $flashData;
        }

        return $data;
    }
}
