<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Core;

use Ecommerce\Infrastructure\Domain\Service\Service;

/**
 * Class BaseController
 * @package Ecommerce\Infrastructure\Core
 */
abstract class BaseController
{
    /**
     * Classe de serviço
     *
     * @var string
     */
    protected $serviceClass = BaseService::class;

    /**
     * @return \Ecommerce\Infrastructure\Domain\Service\Service
     */
    protected function newService(): Service
    {
        return new $this->serviceClass;
    }
}