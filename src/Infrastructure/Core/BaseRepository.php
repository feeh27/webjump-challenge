<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Core;

/**
 * Class BaseRepository
 * @package Ecommerce\Infrastructure\Core
 */
abstract class BaseRepository
{
    /**
     * @var string
     */
    protected $modelClass = BaseModel::class;

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @return \Ecommerce\Infrastructure\Core\BaseModel
     */
    protected function newModel(): BaseModel
    {
        return new $this->modelClass;
    }

    /**
     * @param array $filters
     * @return void
     */
    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }
}
