<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package Ecommerce\Infrastructure\Core
 */
abstract class BaseModel extends Model
{

}