<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure\Core;

/**
 * Class BaseService
 * @package Ecommerce\Infrastructure\Core
 */
abstract class BaseService
{
    /**
     * Classe do repositório
     *
     * @var string
     */
    protected $repositoryClass = BaseRepository::class;

    /**
     * Classe de validação
     */
    protected $validationClass = BaseValidation::class;

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @return \Ecommerce\Infrastructure\Core\BaseRepository
     */
    protected function newRepository(): BaseRepository
    {
        return new $this->repositoryClass;
    }

    /**
     * @return \Ecommerce\Infrastructure\Core\BaseValidation
     */
    protected function newValidation(): BaseValidation
    {
        return new $this->validationClass;
    }

    /**
     * @param array $filters
     * @return void
     */
    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }
}
