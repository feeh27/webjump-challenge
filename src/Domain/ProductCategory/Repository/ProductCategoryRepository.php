<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductCategory\Repository;

use Ecommerce\Domain\ProductCategory\ProductCategory;
use Ecommerce\Infrastructure\Domain\Repository\CrudRepository;
use Ecommerce\Units\Library\Repository\EloquentCrudRepository;

/**
 * Class ProductCategoryRepository
 * @package Ecommerce\Domain\ProductCategory\Repository
 */
class ProductCategoryRepository extends CrudRepository
{
    /**
     * {@inheritdoc}
     */
    protected $modelClass = ProductCategory::class;

    /**
     * {@inheritdoc}
     */
    protected $repositoryClass = EloquentCrudRepository::class;
}
