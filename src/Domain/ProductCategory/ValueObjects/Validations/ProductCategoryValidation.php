<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductCategory\ValueObjects\Validation;

use Ecommerce\Infrastructure\Domain\Validation\Validation;

/**
 * Class ProductCategoryValidation
 * @package Ecommerce\Domain\ProductCategory\ValueObjects\Validation
 */
class ProductCategoryValidation extends Validation
{
    /**
     * @param array $data
     * @return array
     */
    public function getCreateValidation(array $data): array
    {
        $validation = []; // Todo: Validar produtos_categorias

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getFirstOrCreateValidation(array $data): array
    {
        $validation = []; // Todo: Validar produtos_categorias

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getUpdateValidation(array $data): array
    {
        $validation = []; // Todo: Validar produtos_categorias

        return $validation;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        $messages = [];

        return $messages;
    }

    /**
     * @return array
     */
    public function getCustomAttr(): array
    {
        $attr = [];

        return $attr;
    }
}
