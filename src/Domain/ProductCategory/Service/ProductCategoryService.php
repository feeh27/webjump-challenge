<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductCategory\Service;

use Ecommerce\Domain\ProductCategory\Repository\ProductCategoryRepository;
use Ecommerce\Domain\ProductCategory\ValueObjects\Validation\ProductCategoryValidation;
use Ecommerce\Infrastructure\Domain\Service\CrudService;

/**
 * Class ProductCategoryService
 * @package Ecommerce\Domain\ProductCategory\Service
 */
class ProductCategoryService extends CrudService
{
    /**
     * {@inheritDoc}
     */
    protected $repositoryClass = ProductCategoryRepository::class;

    /**
     * {@inheritDoc}
     */
    protected $validationClass = ProductCategoryValidation::class;
}
