<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductCategory;

use Ecommerce\Infrastructure\Domain\Model\Model;

/**
 * Class ProductCategory
 * @package Ecommerce\Domain\ProductCategory
 *
 * @property int $id
 * @property int $product_id
 * @property int $category_id
 */
class ProductCategory extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'product_category';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'product_id',
        'category_id',
    ];

    /**
     * @var array
     */
    public $enabledFilters = [
        'product_id',
        'category_id',
    ];
}