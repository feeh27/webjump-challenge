<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductCategory\Database\Migration;

use Ecommerce\Infrastructure\Domain\Migration\Contracts\MigrationContract;
use Ecommerce\Units\Library\Database\Migration\EloquentMigration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ProductCategoryMigration
 * @package Ecommerce\Domain\ProductCategory\Database\Migration
 */
class ProductCategoryMigration extends EloquentMigration implements MigrationContract
{
    /**
     * @var string
     */
    protected $table = 'product_category';

    /**
     * {@inheritDoc}
     */
    public function migrate(): void
    {
        $this->schema->create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            // $table->foreign('product_id')->references('id')->on('products');
            $table->unsignedInteger('category_id');
            // $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function rollback(): void
    {
        $this->schema->dropIfExists($this->table);
    }
}
