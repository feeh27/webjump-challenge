<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductImage\ValueObjects\Validation;

use Ecommerce\Infrastructure\Domain\Validation\Validation;

/**
 * Class ProductImageValidation
 * @package Ecommerce\Domain\ProductImage\ValueObjects\Validation
 */
class ProductImageValidation extends Validation
{
    /**
     * @param array $data
     * @return array
     */
    public function getCreateValidation(array $data): array
    {
        $validation = []; // Todo: Validar produtos_categorias

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getFirstOrCreateValidation(array $data): array
    {
        $validation = []; // Todo: Validar produtos_categorias

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getUpdateValidation(array $data): array
    {
        $validation = []; // Todo: Validar produtos_categorias

        return $validation;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        $messages = [];

        return $messages;
    }

    /**
     * @return array
     */
    public function getCustomAttr(): array
    {
        $attr = [];

        return $attr;
    }
}
