<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductImage;

use Ecommerce\Infrastructure\Domain\Model\Model;

/**
 * Class ProductImage
 * @package Ecommerce\Domain\ProductImage
 *
 * @property int    $id
 * @property string $name
 * @property int    $product_id
 * @property string $url
 */
class ProductImage extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'product_image';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'name',
        'product_id',
        'url',
    ];

    /**
     * @var array
     */
    public $enabledFilters = [
        'name',
        'product_id',
    ];
}