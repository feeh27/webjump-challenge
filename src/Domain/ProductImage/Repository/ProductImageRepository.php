<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductImage\Repository;

use Ecommerce\Domain\ProductImage\ProductImage;
use Ecommerce\Infrastructure\Domain\Repository\CrudRepository;
use Ecommerce\Units\Library\Repository\EloquentCrudRepository;

/**
 * Class ProductImageRepository
 * @package Ecommerce\Domain\ProductImage\Repository
 */
class ProductImageRepository extends CrudRepository
{
    /**
     * {@inheritdoc}
     */
    protected $modelClass = ProductImage::class;

    /**
     * {@inheritdoc}
     */
    protected $repositoryClass = EloquentCrudRepository::class;
}
