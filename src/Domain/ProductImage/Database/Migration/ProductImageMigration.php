<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductImage\Database\Migration;

use Ecommerce\Infrastructure\Domain\Migration\Contracts\MigrationContract;
use Ecommerce\Units\Library\Database\Migration\EloquentMigration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ProductImageMigration
 * @package Ecommerce\Domain\ProductImage\Database\Migration
 */
class ProductImageMigration extends EloquentMigration implements MigrationContract
{
    /**
     * @var string
     */
    protected $table = 'product_image';

    /**
     * {@inheritDoc}
     */
    public function migrate(): void
    {
        $this->schema->create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->unsignedInteger('product_id');
            // $table->foreign('product_id')->references('id')->on('products');
            $table->string('url', 200);
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function rollback(): void
    {
        $this->schema->dropIfExists($this->table);
    }
}
