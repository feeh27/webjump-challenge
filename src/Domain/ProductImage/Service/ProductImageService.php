<?php

declare(strict_types=1);

namespace Ecommerce\Domain\ProductImage\Service;

use Ecommerce\Domain\ProductImage\ProductImage;
use Ecommerce\Domain\ProductImage\Repository\ProductImageRepository;
use Ecommerce\Domain\ProductImage\ValueObjects\Validation\ProductImageValidation;
use Ecommerce\Infrastructure\Domain\Service\CrudService;

/**
 * Class ProductImageService
 * @package Ecommerce\Domain\ProductImage\Service
 */
class ProductImageService extends CrudService
{
    /**
     * {@inheritDoc}
     */
    protected $repositoryClass = ProductImageRepository::class;

    /**
     * {@inheritDoc}
     */
    protected $validationClass = ProductImageValidation::class;

    /**
     * @param int $product_id
     * @param string $name
     * @return null|\Ecommerce\Domain\ProductImage\ProductImage
     */
    public function getByProductIdAndName(int $product_id, string $name): ?ProductImage
    {
        /**
         * @var \Illuminate\Support\Collection $piCollection
         */
        $piCollection = $this->get()['data'];

        $pi = $piCollection->filter(function (ProductImage $pi) use($product_id, $name) {
            return $pi->product_id === $product_id && $pi->name === $name;
        });

        return $pi->first();
    }
}
