<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Categories\Service;

use Ecommerce\Domain\Categories\Repository\CategoryRepository;
use Ecommerce\Domain\Categories\ValueObjects\Validation\CategoryValidation;
use Ecommerce\Domain\ProductCategory\ProductCategory;
use Ecommerce\Domain\ProductCategory\Service\ProductCategoryService;
use Ecommerce\Infrastructure\Domain\Service\CrudService;

/**
 * Class Service
 * @package Ecommerce\Domain\s\Service
 */
class CategoryService extends CrudService
{
    /**
     * {@inheritDoc}
     */
    protected $repositoryClass = CategoryRepository::class;

    /**
     * {@inheritDoc}
     */
    protected $validationClass = CategoryValidation::class;

    /**
     * {@inheritdoc}
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function delete($id): bool
    {
        $pcService = new ProductCategoryService();

        /**
         * @var \Illuminate\Support\Collection $pcCollection
         */
        $pcCollection = $pcService->get()['data'];

        $data = $pcCollection->filter(function (ProductCategory $pc) use ($id) {
            return $pc->category_id === (int) $id;
        });

        if ($data->count() > 0) {
            session()->writeFlash('message', [
                'type' => 'error',
                'text' => 'This category is linked to one or more products!'
            ]);

            response()->redirect(url('categories')->getPath(), 422);

            return false;
        }

        return parent::delete($id);
    }
}
