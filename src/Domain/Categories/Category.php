<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Categories;

use Ecommerce\Domain\Products\Product;
use Ecommerce\Infrastructure\Domain\Model\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package Ecommerce\Domain\Categories
 *
 * @property int $id
 * @property string name
 */
class Category extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
    ];

    /**
     * @var array
     */
    public $enabledFilters = [
        'id',
        'name',
        'code',
    ];

    /**
     * The products that belong to the category.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }
}
