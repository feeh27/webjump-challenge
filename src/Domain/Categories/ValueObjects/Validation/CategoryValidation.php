<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Categories\ValueObjects\Validation;

use Ecommerce\Infrastructure\Domain\Validation\Validation;

/**
 * Class CategoryValidation
 * @package Ecommerce\Domain\Categories\ValueObjects\Validation
 */
class CategoryValidation extends Validation
{
    /**
     * @param array $data
     * @return array
     */
    public function getCreateValidation(array $data): array
    {
        $validation = [
            'name' => 'required',
            'code' => 'required|unique:categories,code',
        ];

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getFirstOrCreateValidation(array $data): array
    {
        $validation = [
            'name' => 'required',
            'code' => 'required',
        ];

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getUpdateValidation(array $data): array
    {
        $validation = [
            'name' => 'sometimes|required',
            'code' => 'sometimes|required|unique:categories,code,' . $data['id'],
        ];

        return $validation;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        $messages = [];

        return $messages;
    }

    /**
     * @return array
     */
    public function getCustomAttr(): array
    {
        $attr = [];

        return $attr;
    }
}
