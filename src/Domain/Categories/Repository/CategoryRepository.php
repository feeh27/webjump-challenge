<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Categories\Repository;

use Ecommerce\Domain\Categories\Category;
use Ecommerce\Infrastructure\Domain\Repository\CrudRepository;
use Ecommerce\Units\Library\Repository\EloquentCrudRepository;

/**
 * Class CategoryRepository
 * @package Ecommerce\Domain\Categories\Repository
 */
class CategoryRepository extends CrudRepository
{
    /**
     * {@inheritdoc}
     */
    protected $modelClass = Category::class;

    /**
     * {@inheritdoc}
     */
    protected $repositoryClass = EloquentCrudRepository::class;
}
