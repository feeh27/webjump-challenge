<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Categories\Database\Migration;

use Ecommerce\Infrastructure\Domain\Migration\Contracts\MigrationContract;
use Ecommerce\Units\Library\Database\Migration\EloquentMigration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CategoryMigration
 * @package Ecommerce\Domain\Categories\Migration
 */
class CategoryMigration extends EloquentMigration implements MigrationContract
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * {@inheritDoc}
     */
    public function migrate(): void
    {
        $this->schema->create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function rollback(): void
    {
        $this->schema->dropIfExists($this->table);
    }
}