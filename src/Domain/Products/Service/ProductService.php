<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Products\Service;

use Ecommerce\Domain\ProductImage\Service\ProductImageService;
use Ecommerce\Domain\Products\Repository\ProductRepository;
use Ecommerce\Domain\Products\ValueObjects\Validation\ProductValidation;
use Ecommerce\Infrastructure\Domain\Service\CrudService;
use Ecommerce\Units\Library\File\Image\ImageUploader;
use Ecommerce\Units\Library\Log\Logger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pecee\Http\Input\InputFile;

/**
 * Class ProductService
 * @package Ecommerce\Domain\Products\Service
 */
class ProductService extends CrudService
{
    /**
     * {@inheritDoc}
     */
    protected $repositoryClass = ProductRepository::class;

    /**
     * {@inheritDoc}
     */
    protected $validationClass = ProductValidation::class;

    /**
     * {@inheritDoc}
     * @return \Ecommerce\Domain\Products\Product
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function create(array $data): Model
    {
        /**
         * @var \Ecommerce\Domain\Products\Product $product
         */
        $product = parent::create($data);

        $product->categories()->sync($data['categories']);

        $logMessage = 'Sync categories from product';
        $logData = [
            'product_id' => $product->id,
            'categories' => $data['categories'],
        ];

        $logger = new Logger();
        $logger->logDbActions($logMessage, 'product', 'CREATE', $logData);

        if (isset($data['image']) && $data['image'] !== null && $data['image'] instanceof InputFile) {
            $imageUploader = new ImageUploader($data);
            $image = [];
            $image['name'] = 'profile';
            $image['product_id'] = $product->id;
            $image['url'] = $imageUploader->imageToUrl('products', $product->id);

            $piService = new ProductImageService();

            $piService->create($image);
        }

        return $product;
    }

    /**
     * {@inheritDoc}
     * @return \Ecommerce\Domain\Products\Product
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    public function update($id, array $data): Model
    {
        /**
         * @var \Ecommerce\Domain\Products\Product $product
         */
        $product = parent::update($id, $data);

        $product->categories()->sync($data['categories']);

        $logMessage = 'Sync categories from product';
        $logData = [
            'product_id' => $id,
            'categories' => $data['categories'],
        ];

        $logger = new Logger();
        $logger->logDbActions($logMessage, 'product', 'UPDATE', $logData);

        if (isset($data['image']) && $data['image'] !== null && $data['image'] instanceof InputFile) {
            $imageUploader = new ImageUploader($data);

            $image = [
                'name' => 'profile',
                'url' => $imageUploader->imageToUrl('products', $product->id),
                'product_id' => $id,
            ];

            $piService = new ProductImageService();

            try {
                $pi = $piService->getByProductIdAndName($product->id, 'profile');
                $piService->update($pi->id, $image);
            } catch (ModelNotFoundException $e) {
                $piService->create($image);
            }
        }

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id): bool
    {
        /**
         * @var \Ecommerce\Domain\Products\Product $product
         */
        $product = $this->getById($id);

        $categories = $product->categories->pluck('id')->toArray();

        $product->categories()->detach($categories);

        $logMessage = 'Detach categories from product';
        $logData = [
            'product_id' => $id,
            'categories' => $categories,
        ];

        $logger = new Logger();
        $logger->logDbActions($logMessage, 'product', 'DELETE', $logData);

        return parent::delete($id);
    }
}
