<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Products\Repository;

use Ecommerce\Domain\Products\Product;
use Ecommerce\Infrastructure\Domain\Repository\CrudRepository;
use Ecommerce\Units\Library\Repository\EloquentCrudRepository;

/**
 * Class ProductRepository
 * @package Ecommerce\Domain\Products\Repository
 */
class ProductRepository extends CrudRepository
{
    /**
     * {@inheritdoc}
     */
    protected $modelClass = Product::class;

    /**
     * {@inheritdoc}
     */
    protected $repositoryClass = EloquentCrudRepository::class;
}
