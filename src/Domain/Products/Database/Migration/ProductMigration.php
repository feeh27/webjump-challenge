<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Products\Database\Migration;

use Ecommerce\Infrastructure\Domain\Migration\Contracts\MigrationContract;
use Ecommerce\Units\Library\Database\Migration\EloquentMigration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ProductMigration
 * @package Ecommerce\Domain\Product\Migration
 */
class ProductMigration extends EloquentMigration implements MigrationContract
{
    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * {@inheritDoc}
     */
    public function migrate(): void
    {
        $this->schema->create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('sku');
            $table->decimal('price', 10, 2);
            $table->string('description');
            $table->integer('quantity');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function rollback(): void
    {
        $this->schema->dropIfExists($this->table);
    }
}