<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Products\ValueObjects\Validation;

use Ecommerce\Infrastructure\Domain\Validation\Validation;

/**
 * Class ProductValidation
 * @package Ecommerce\Domain\Products\ValueObjects\Validation
 */
class ProductValidation extends Validation
{
    /**
     * @param array $data
     * @return array
     */
    public function getCreateValidation(array $data): array
    {
        $validation = [
            'sku' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'description' => 'required',
            'categories' => 'required|array',
            'categories.*' => 'required|integer',
        ];

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getFirstOrCreateValidation(array $data): array
    {
        $validation = [
            'sku' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'description' => 'required',
            'categories' => 'required|array',
            'categories.*' => 'required|integer',
        ];

        return $validation;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getUpdateValidation(array $data): array
    {
        $validation = [
            'sku' => 'sometimes|required',
            'name' => 'sometimes|required',
            'price' => 'sometimes|required|numeric',
            'quantity' => 'sometimes|required|integer',
            'description' => 'sometimes|required',
            'categories' => 'required|array',
            'categories.*' => 'sometimes|required|integer',
        ];

        return $validation;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        $messages = [];

        return $messages;
    }

    /**
     * @return array
     */
    public function getCustomAttr(): array
    {
        $attr = [];

        return $attr;
    }
}
