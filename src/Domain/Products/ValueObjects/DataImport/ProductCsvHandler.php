<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Products\ValueObjects\DataImport;

use Ecommerce\Domain\Categories\Service\CategoryService;
use Ecommerce\Domain\Products\Service\ProductService;
use Ecommerce\Infrastructure\Domain\File\Csv\Contracts\CsvExternalMapContract;
use Ecommerce\Infrastructure\Domain\File\Csv\ModelCsvHandler;
use Ecommerce\Units\Library\File\Csv\CsvDbMap;
use Ecommerce\Units\Library\File\Csv\CsvExternalMap;
use Ecommerce\Units\Library\File\DataImportRepository;

/**
 * Class ProductCsvHandler
 * @package Ecommerce\Domain\Products\Importer
 */
class ProductCsvHandler extends ModelCsvHandler
{
    /**
     * @var string
     */
    protected $serviceClass = ProductService::class;

    /**
     * @var string
     */
    protected $filepath = 'import.csv';

    /**
     * @var string
     */
    protected $delimiter = ';';

    /**
     * @var array
     */
    protected $fields = [
        'nome',
        'sku',
        'descricao',
        'preco',
        'categoria'
    ];

    /**
     * // Campo do CSV => nome do campo
     * @var array
     */
    protected $map = [];

    /**
     * // Campo do CSV => [callback => nome da funcao, delimiter => delimitador (para múltiplos)]
     * @var array
     */
    protected $externalMap = [];

    /**
     * ProductCsvHandler constructor.
     */
    public function __construct()
    {
        $this->populateMap();
        $this->populateExternalMap();
    }

    /**
     * {@inheritDoc}
     */
    protected function populateMap(): void
    {
        $this->map[] = new CsvDbMap('nome', 'name');
        $this->map[] = new CsvDbMap('sku', 'sku');
        $this->map[] = new CsvDbMap('preco', 'price');
        $this->map[] = new CsvDbMap('descricao', 'description');
        $this->map[] = new CsvDbMap('quantidade', 'quantity');
        $this->map[] = new CsvDbMap('categorias', 'categories');
    }

    /**
     * {@inheritDoc}
     */
    protected function populateExternalMap(): void
    {
        $this->externalMap[] = new CsvExternalMap('categoria', '|');
    }

    /**
     * {@inheritDoc}
     * @throws \Exception
     */
    public function process(array $record): bool
    {
        $repository = new DataImportRepository();

        $success = $repository->process('product', function () use ($record) {

            $record['categorias'] = $this->processCategory($record);

            $this->addFromRecord($record);

            // $product->categories()->sync($categories);
        });

        return $success;
    }

    /**
     * @param array $record
     * @return array
     * @throws \Ecommerce\Units\Exceptions\Http\ValidationException
     */
    private function processCategory(array $record): array
    {
        $categories_id = [];

        /**
         * @var \Ecommerce\Domain\Products\Product $product
         * @var \Ecommerce\Domain\Categories\Category $category
         */
        $externalFields = collect($this->externalMap);

        $externalField = $externalFields->filter(function (CsvExternalMapContract $csvExternalMap) {
            return $csvExternalMap->getCsvField() === 'categoria';
        })->first();

        if ($externalField instanceof CsvExternalMapContract) {

            $delimiter = $externalField->getDelimiter();

            $names = $record[$externalField->getCsvField()];

            if ($names === '') {
                $names = '(no category listed)';
            }

            $categories = explode($delimiter, $names);

            /**
             * @var \Ecommerce\Infrastructure\Domain\Service\CrudService $categoryService
             */
            $categoryService = new CategoryService();

            foreach ($categories as $categoryName) {
                $categoryData = [
                    'name' => $categoryName,
                    'code' => $categoryName,
                ];

                $category = $categoryService->firstOrCreate($categoryData);

                $categories_id[] = $category->id;
            }
        }

        return $categories_id;
    }
}
