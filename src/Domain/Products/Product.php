<?php

declare(strict_types=1);

namespace Ecommerce\Domain\Products;

use Ecommerce\Domain\Categories\Category;
use Ecommerce\Domain\ProductImage\ProductImage;
use Ecommerce\Infrastructure\Domain\Model\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package Ecommerce\Domain\Products
 *
 * @property int $id
 * @property string name
 * @property string sku
 * @property double price
 * @property string description
 * @property int quantity
 *
 * @property \Illuminate\Support\Collection categories
 */
class Product extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'sku',
        'price',
        'description',
        'quantity',
    ];

    /**
     * @var array
     */
    public $enabledFilters = [
        'id',
        'name',
        'sku',
        'price',
        'description',
        'quantity',
    ];

    /**
     * The categories that belong to the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    /**
     * The categories that belong to the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }
}
