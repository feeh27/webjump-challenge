#! /usr/bin/env php
<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Symfony\Component\Console\Application();

$app->add(new Ecommerce\Units\Console\Commands\Migrator\Migrate());
$app->add(new Ecommerce\Units\Console\Commands\Migrator\Rollback());
$app->add(new Ecommerce\Units\Console\Commands\Migrator\Reset());
$app->add(new Ecommerce\Units\Console\Commands\DataImport\CsvImport());

$app->run();
