<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Library\Repository;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Library\Repository\EloquentRepositoryFilter;

/**
 * Class EloquentRepositoryFilter
 * @package Ecommerce\Test\Unit\Units\Library\Repository
 */
class EloquentRepositoryFilterTest extends TestCase
{
    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function verify_if_getLimit_method_works_default_value()
    {
        $expected = null;

        $filters = [];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->getLimit();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function verify_if_getLimit_method_works_filter_value()
    {
        $expected = 10;

        $limit = 10;

        $filters = [
            'limit' => $limit
        ];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->getLimit();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function verify_if_isPaginate_method_works_default_value()
    {
        // Test 1 - Default value
        $expected = false;

        $filters = [];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->isPaginate();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function verify_if_isPaginate_method_works_filter_value()
    {
        // Test 2 - Filter value
        $page = 3;
        $expected = true;

        $filters = [
            'page' => $page,
        ];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->isPaginate();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function verify_if_getSort_method_works_default_value()
    {
        // Test 1 - Default value
        $keyName = 'id';
        $expectedSort = $keyName;
        $filters = [];

        $repositoryFilter = new EloquentRepositoryFilter($keyName, $filters);

        $actualSort = $repositoryFilter->getSort();

        $this->assertEquals($expectedSort, $actualSort);
    }

    /**
     * @test
     */
    public function verify_if_getSort_method_works_filter_value()
    {
        // Test 2 - Filter value
        $keyName = 'id';
        $expectedSort = 'name';

        $filters = [
            'sort' => $expectedSort,
        ];

        $repositoryFilter = new EloquentRepositoryFilter($keyName, $filters);

        $actualSort = $repositoryFilter->getSort();

        $this->assertEquals($expectedSort, $actualSort);
    }

    /**
     * @test
     */
    public function verify_if_getOrder_method_works_default_value()
    {
        $expected = 'ASC';

        $filters = [];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->getOrder();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function verify_if_getOrder_method_works_filter_value()
    {
        $expected = 'DESC';

        $order = 'DESC';

        $filters = [
            'order' => $order
        ];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->getOrder();

        $this->assertEquals($expected, $actual);
    }
    /**
     * @test
     */
    public function verify_if_getOrder_method_works_invalid_value()
    {
        $expected = 'ASC';

        $order = 10;

        $filters = [
            'order' => $order
        ];

        $repositoryFilter = new EloquentRepositoryFilter('id', $filters);

        $actual = $repositoryFilter->getOrder();

        $this->assertEquals($expected, $actual);
    }
}
