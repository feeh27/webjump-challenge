<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Library\File\Csv;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Library\File\Csv\CsvProcessResult;

/**
 * Class CsvProcessResultTest
 * @package Ecommerce\Test\Unit\Units\Library\File\Csv
 */
class CsvProcessResultTest extends TestCase
{
    /**
     * @test
     */
    public function test_constructor()
    {
        $expectedA = 10;
        $expectedB = 20;

        $csvProcessResult = new CsvProcessResult($expectedA, $expectedB);

        $actualA = $csvProcessResult->getRows();
        $actualB = $csvProcessResult->getRegisters();

        $this->assertEquals($expectedA, $actualA);
        $this->assertEquals($expectedB, $actualB);
    }

    /**
     * @test
     */
    public function test_rows_setter()
    {
        $expected = 15;

        $csvProcessResult = new CsvProcessResult(0, 0);

        $csvProcessResult->setRows($expected);
        $actual = $csvProcessResult->getRows();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_registers_setter()
    {
        $expected = 30;

        $csvProcessResult = new CsvProcessResult(0, 0);

        $csvProcessResult->setRegisters($expected);
        $actual = $csvProcessResult->getRegisters();

        $this->assertEquals($expected, $actual);
    }

}
