<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Library\File\Csv;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Library\File\Csv\CsvMap;

/**
 * Class CsvDbMapTest
 * @package Ecommerce\Test\Unit\Units\Library\File\Csv
 */
class CsvMapTest extends TestCase
{
    /**
     * @test
     */
    public function test_constructor()
    {
        $expected = 'aaa123';

        $csvDbMap = new CsvMap($expected);

        $actual = $csvDbMap->getCsvField();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_csvField_setter()
    {
        $expected = 'aaa123';

        $csvDbMap = new CsvMap('');

        $csvDbMap->setCsvField($expected);
        $actual = $csvDbMap->getCsvField();

        $this->assertEquals($expected, $actual);
    }
}
