<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Library\File\Csv;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Library\File\Csv\CsvExternalMap;

/**
 * Class CsvDbMapTest
 * @package Ecommerce\Test\Unit\Units\Library\File\Csv
 */
class CsvExternalMapTest extends TestCase
{
    /**
     * @test
     */
    public function test_constructor()
    {
        $expectedA = 'aaa123';
        $expectedB = ';';

        $csvDbMap = new CsvExternalMap($expectedA, $expectedB);

        $actualA = $csvDbMap->getCsvField();
        $actualB = $csvDbMap->getDelimiter();

        $this->assertEquals($expectedA, $actualA);
        $this->assertEquals($expectedB, $actualB);
    }

    /**
     * @test
     */
    public function test_csvField_setter()
    {
        $expected = 'aaa123';

        $csvDbMap = new CsvExternalMap('', '');

        $csvDbMap->setCsvField($expected);
        $actual = $csvDbMap->getCsvField();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_delimiter_setter()
    {
        $expected = ';';

        $csvDbMap = new CsvExternalMap('', '');

        $csvDbMap->setDelimiter($expected);
        $actual = $csvDbMap->getDelimiter();

        $this->assertEquals($expected, $actual);
    }
}
