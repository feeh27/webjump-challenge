<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Library\File\Csv;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Library\File\Csv\CsvDbMap;

/**
 * Class CsvDbMapTest
 * @package Ecommerce\Test\Unit\Units\Library\File\Csv
 */
class CsvDbMapTest extends TestCase
{
    /**
     * @test
     */
    public function test_constructor()
    {
        $expectedA = 'aaa123';
        $expectedB = 'xxx456';

        $csvDbMap = new CsvDbMap($expectedA, $expectedB);

        $actualA = $csvDbMap->getCsvField();
        $actualB = $csvDbMap->getDbField();

        $this->assertEquals($expectedA, $actualA);
        $this->assertEquals($expectedB, $actualB);
    }

    /**
     * @test
     */
    public function test_csvField_setter()
    {
        $expected = 'aaa123';

        $csvDbMap = new CsvDbMap('', '');

        $csvDbMap->setCsvField($expected);
        $actual = $csvDbMap->getCsvField();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_dbField_setter()
    {
        $expected = 'aaa123';

        $csvDbMap = new CsvDbMap('', '');

        $csvDbMap->setDbField($expected);
        $actual = $csvDbMap->getDbField();

        $this->assertEquals($expected, $actual);
    }
}
