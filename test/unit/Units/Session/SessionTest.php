<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Session;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Exceptions\Http\SessionException;
use Ecommerce\Units\Session\Session;
use Exception;

/**
 * Class SessionTest
 * @package Ecommerce\Test\Unit\Units\Session
 */
class SessionTest extends TestCase
{
    /**
     * @var \Ecommerce\Units\Session\Session
     */
    private $session;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->session = new Session();
    }

	/**
	 * @test
	 */
	public function verify_if_session_is_not_started()
	{
		$session_started = $this->session->isStarted();

		$this->assertFalse($session_started);
	}

	/**
	 * @test
	 */
	public function verify_exception_when_session_is_not_started()
	{
		try {
			$data = $this->session->all();

			$this->assertIsArray($data);
		} catch (Exception $e) {
			$this->assertTrue($e instanceof SessionException);
		}
	}

    /**
     * @test
     */
    public function verify_if_session_start()
    {
		$this->session = new Session(true);

        $session_started = $this->session->isStarted();

        $this->assertTrue($session_started);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_method_all_return_array()
    {
        $data = $this->session->all();

        $this->assertIsArray($data);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_write_and_read_methods_works_correctly()
    {
        $expected = 'test';

        $this->session->write('aaaa', $expected);

        $actual = $this->session->read('aaaa');
        $nullData = $this->session->read('bbbb');

        $this->assertEquals($expected, $actual);

        $this->assertNull($nullData);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_remove_method_works_correctly()
    {
		$this->session->remove('aaaa');

        $nullData = $this->session->read('aaaa');

        $this->assertNull($nullData);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_method_allFlash_return_array()
    {
		$data = $this->session->allFlash();

        $this->assertIsArray($data);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_write_and_read_flash_methods_works_correctly()
    {
		$expected = ['aa' => 'test'];

        $this->session->writeFlash('aaaa', $expected);

        $actual = $this->session->readFlash('aaaa');

        $this->assertArrayHasKey('timestamp', $actual);
        $this->assertArrayHasKey('aa', $actual);
        $this->assertEquals($expected['aa'], $actual['aa']);

        $nullData = $this->session->readFlash('bbbb');
        $this->assertNull($nullData);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_method_allOldFlash_return_array()
    {
		$data = $this->session->allOldFlash();

        $this->assertIsArray($data);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_read_old_flash_method_works_correctly()
    {
		$nullData = $this->session->readOldFlash('bbbb');
        $this->assertNull($nullData);
    }

    /**
     * @test
     * @throws \Ecommerce\Units\Exceptions\Http\SessionException
     */
    public function verify_if_handleDestruct_method_works_correctly()
    {
		// ===== Primeira parte =====

        $key = 'aaaa';
        $expected = ['aa' => 'test'];

        $this->session->writeFlash($key, $expected);

        $readFlash1 = $this->session->readFlash($key);
        $readOldFlash1 = $this->session->readOldFlash($key);

        $this->assertEquals($expected['aa'], $readFlash1['aa']);
        $this->assertNull($readOldFlash1);

        // ===== Segunda parte =====

        $this->session->handleDestruct();

        $readFlash2 = $this->session->readFlash($key);
        $readOldFlash2 = $this->session->readOldFlash($key);

        $this->assertEquals($expected['aa'], $readFlash2['aa']);
        $this->assertEquals($expected['aa'], $readOldFlash2['aa']);

        // ===== Terceira parte =====

        $this->session->handleDestruct();

        $readFlash3 = $this->session->readFlash($key);
        $readOldFlash3 = $this->session->readOldFlash($key);

        $this->assertNull($readFlash3);
        $this->assertEquals($expected['aa'], $readOldFlash3['aa']);

        // ===== Quarta parte =====

        $this->session->handleDestruct();

        $readFlash4 = $this->session->readFlash($key);
        $readOldFlash4 = $this->session->readOldFlash($key);

        $this->assertNull($readFlash4);
        $this->assertNull($readOldFlash4);
    }
}
