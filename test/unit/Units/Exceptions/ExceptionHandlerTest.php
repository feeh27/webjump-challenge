<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Exceptions;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Exceptions\ExceptionHandler;
use Ecommerce\Units\Exceptions\Http\ForbiddenException;
use Ecommerce\Units\Exceptions\Http\MethodNotAllowedException;
use Ecommerce\Units\Exceptions\Http\UnauthorizedException;
use Ecommerce\Units\Exceptions\Http\ValidationException;
use Exception;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;

/**
 * Class ExceptionHandlerTest
 * @package Ecommerce\Test\Unit\Units\Exceptions
 */
class ExceptionHandlerTest extends TestCase
{
    /**
     * @var \Pecee\Http\Request
     */
    private $request;

    /**
     * @var \Ecommerce\Units\Exceptions\ExceptionHandler
     */
    private $exceptionHandler;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->request = request();
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * @test
     */
    public function test_401_exception(): void
    {
        $expected = 'Ecommerce\Units\Controllers\ErrorsController@unauthorized';

        try {
            throw new UnauthorizedException();
        } catch (Exception $e) {
            $actual = $this->exceptionHandler->getErrorControllerMethod($e);
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_403_exception(): void
    {
        $expected = 'Ecommerce\Units\Controllers\ErrorsController@forbidden';

        try {
            throw new ForbiddenException();
        } catch (Exception $e) {
            $actual = $this->exceptionHandler->getErrorControllerMethod($e);
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_404_exception(): void
    {
        $expected = 'Ecommerce\Units\Controllers\ErrorsController@notFound';

        try {
            throw new NotFoundHttpException();
        } catch (Exception $e) {
            $actual = $this->exceptionHandler->getErrorControllerMethod($e);
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_405_exception(): void
    {
        $expected = 'Ecommerce\Units\Controllers\ErrorsController@methodNotAllowed';

        try {
            throw new MethodNotAllowedException();
        } catch (Exception $e) {
            $actual = $this->exceptionHandler->getErrorControllerMethod($e);
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_422_exception(): void
    {
        $expected = 'Ecommerce\Units\Controllers\ErrorsController@unprocessableEntity';

        try {
            throw new ValidationException();
        } catch (Exception $e) {
            $actual = $this->exceptionHandler->getErrorControllerMethod($e);
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function test_503_exception(): void
    {
        $expected = 'Ecommerce\Units\Controllers\ErrorsController@serviceUnavailable';

        try {
            throw new Exception();
        } catch (Exception $e) {
            $actual = $this->exceptionHandler->getErrorControllerMethod($e);
        }

        $this->assertEquals($expected, $actual);
    }
}
