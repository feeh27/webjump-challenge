<?php

declare(strict_types=1);

namespace Ecommerce\Test\Unit\Units\Exceptions\Http;

use Ecommerce\Test\Unit\TestCase;
use Ecommerce\Units\Exceptions\Http\ForbiddenException;
use Ecommerce\Units\Exceptions\Http\MethodNotAllowedException;
use Ecommerce\Units\Exceptions\Http\UnauthorizedException;
use Ecommerce\Units\Exceptions\Http\ValidationException;
use Exception;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;

/**
 * Class HttpExceptionsTest
 * @package Ecommerce\Test\Unit\Units\Exceptions\Http
 */
class HttpExceptionsTest extends TestCase
{
    /**
     * @test
     */
    public function test_401_exception(): void
    {
        try {
            throw new UnauthorizedException();
        } catch (Exception $e) {
            $this->assertTrue($e instanceof UnauthorizedException);
        }
    }

    /**
     * @test
     */
    public function test_403_exception(): void
    {
        try {
            throw new ForbiddenException();
        } catch (Exception $e) {
            $this->assertTrue($e instanceof ForbiddenException);
        }
    }

    /**
     * @test
     */
    public function test_404_exception(): void
    {
        try {
            throw new NotFoundHttpException();
        } catch (Exception $e) {
            $this->assertTrue($e instanceof NotFoundHttpException);
        }
    }

    /**
     * @test
     */
    public function test_405_exception(): void
    {
        try {
            throw new MethodNotAllowedException();
        } catch (Exception $e) {
            $this->assertTrue($e instanceof MethodNotAllowedException);
        }
    }

    /**
     * @test
     */
    public function test_422_exception(): void
    {
        try {
            throw new ValidationException();
        } catch (Exception $e) {
            $this->assertTrue($e instanceof ValidationException);
        }
    }

    /**
     * @test
     */
    public function test_503_exception(): void
    {
        try {
            throw new Exception();
        } catch (Exception $e) {
            $this->assertTrue($e instanceof Exception);
        }
    }
}