# Estrutura base para Ecommerce

Esse projeto permite a criação de um Ecommerce simples

[![Build Status](https://scrutinizer-ci.com/b/feeh27/webjump-challenge/badges/build.png?b=master)](https://scrutinizer-ci.com/b/feeh27/webjump-challenge/build-status/master)
[![Code Coverage](https://scrutinizer-ci.com/b/feeh27/webjump-challenge/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/feeh27/webjump-challenge/?branch=master)
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/quality/b/feeh27/webjump-challenge/master)](https://scrutinizer-ci.com/b/feeh27/webjump-challenge/?branch=master)
[![Code Intelligence Status](https://scrutinizer-ci.com/b/feeh27/webjump-challenge/badges/code-intelligence.svg?b=master)](https://scrutinizer-ci.com/code-intelligence)

[![Packagist Version](https://img.shields.io/packagist/v/feeh27/webjump-challenge)](https://packagist.org/packages/feeh27/webjump-challenge)
[![PHP Version](https://img.shields.io/packagist/php-v/feeh27/webjump-challenge?label=php%20version)](https://packagist.org/packages/feeh27/webjump-challenge)

## Requisitos

 - PHP 7.2 ou mais recente
 - Composer

## Instalação

É preferível que seja instalado via [composer](https://getcomposer.org/):

```bash
composer create-project feeh27/ecommerce
```

## Contribuidores desse repositório

Felipe Dominguesche - [Linkedin](https://linkedin.com/in/felipe-dominguesche)
