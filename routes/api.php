<?php

use Ecommerce\Units\Exceptions\ApiExceptionHandler;
use Ecommerce\Units\Library\Http\Middleware\ApiMiddleware;
use Ecommerce\Units\Library\Http\Route\Router;

$router = new Router();

$apiConfig = [
    'prefix' => '/api',
    'middleware' => ApiMiddleware::class,
    'exceptionHandler' => ApiExceptionHandler::class,
];

$router->group($apiConfig, function() use ($router) {
    $router->get('/', function () {
        return 'Home API';
    });

    $router->post('/products', function () {
        return 'Products';
    });

    $router->get('/categories', 'Ecommerce\Units\Controllers\Web\CategoryController@get');
});
