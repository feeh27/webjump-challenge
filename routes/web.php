<?php

use Ecommerce\Units\Library\Http\Middleware\WebMiddleware;
use Ecommerce\Units\Library\Http\Route\Router;

$router = new Router();

$webConfig = [
    'middleware' => WebMiddleware::class,
    'exceptionHandler' => \Ecommerce\Units\Exceptions\ExceptionHandler::class,
    'namespace' => 'Ecommerce\Units\Controllers\Web',
];

$router->group($webConfig, function() use ($router) {
    $router->get('/', 'IndexController@index')->name('home');

	$router->group(['prefix' => '/products'], function () use ($router) {
		$router->get('/', 'ProductController@index')
			->name('products');

		$router->get('/new', 'ProductController@new')
			->name('products.new');

		$router->get('/{id}', 'ProductController@edit')
			->where(['id' => '[0-9]+'])
			->name('products.edit');

		$router->post('/save', 'ProductController@save')
			->where(['id' => '[0-9]+'])
			->name('products.save');

		$router->post('/{id}/save', 'ProductController@save')
			->name('products.save');

		$router->get('/{id}/delete', 'ProductController@delete')
			->where(['id' => '[0-9]+'])
			->name('products.delete');
	});

	$router->group(['prefix' => '/categories'], function () use ($router) {
		$router->get('/', 'CategoryController@index')
			->name('categories');

		$router->get('/new', 'CategoryController@new')
			->name('categories.new');

		$router->get('/{id}', 'CategoryController@edit')
			->where(['id' => '[0-9]+'])
			->name('categories.edit');

		$router->post('/save', 'CategoryController@save')
			->name('categories.save');

		$router->post('/{id}/save', 'CategoryController@save')
			->where(['id' => '[0-9]+'])
			->name('categories.save');

		$router->get('/{id}/delete', 'CategoryController@delete')
			->where(['id' => '[0-9]+'])
			->name('categories.delete');
	});
});
