<?php

return [
    'system' => [
        'timezone' => 'America/Sao_Paulo',
        'date_format' => 'Y-m-d',
        'datetime_format' => 'Y-m-d H:i:s',
    ],
    'db' => [
        'default' => 'mysql',
        'mysql' => [
            'driver' => 'mysql',
            //'host' => '127.0.0.1',
            'host' => 'webjump_db',
            //'port' => '3309',
            'port' => '3306',
            'database' => 'ecommerce',
            'username' => 'dbuser',
            'password' => 'dbpass',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ],
    ],
    'csv' => [
        'folder' => 'assets/'
    ],
];
