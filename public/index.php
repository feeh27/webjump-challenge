<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    \Ecommerce\Units\Library\Http\Route\Router::start();
} catch (Exception $e) {
    $logger = new \Ecommerce\Units\Library\Log\Logger();
    $logger->logException('index.php', $e);
    $exceptionHandler = new \Ecommerce\Units\Exceptions\ExceptionHandler();
    $exceptionHandler->handleError(request(), $e);
}


/**
 * Todo: Documentar como rodar o projeto
 *
 * Todo: Crie uma documentação simples comentando sobre as tecnologias, versões e soluções adotadas
 */
