function message(type, message) {
    Swal.fire({
        position: 'top-end',
        type: type,
        title: message,
        showConfirmButton: false,
        timer: 1000 * 5
    })
}
